\documentclass[a4paper,english]{article}
\usepackage[T1]{fontenc}
\usepackage{babel}
\usepackage{geometry}
\usepackage{natbib}
\usepackage{url}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
 \geometry{
 a4paper,
 total={210mm,297mm},
 left=20mm,
 right=20mm,
 top=20mm,
 bottom=20mm,
 }
\usepackage[affil-it]{authblk}
\title{ Towards Accurate and Scalable Clone Detection using Software Metrics\\Topic Proposal}

\newcommand{\Name}{Oreo}
\newcommand{\AF}{\textit{Action filter}}
\newcommand{\AT}{\textit{Action tokens}}
\newcommand{\HTD}{harder-to-detect}

\begin{document}
\author{
	Vaibhav Saini
	}
\maketitle

\section{Context}
Clone detection is the process of locating exact or similar pieces of
code within or between software systems. Over the past 20 years, clone
detection has been the focus of increasing attention, with many clone
detectors having been proposed and implemented
(see~\cite{Sheneamer:survey16} for a recent survey on this
topic). These clone detection approaches and tools differ from each
other depending on the goals and granularity of the detection. There
are four broad categories of clone detection approaches, ranging from
easy-to-detect to \HTD\ clones: textual similarity, lexical
similarity, syntactic similarity, and semantic similarity. The
literature refers to them as the four commonly accepted types of
clones~\cite{bellon,roy:queens:07}: i) Type-1: Identical code fragments, except for differences in white-space, layout and comments, ii) Type-2: Identical code fragments, except for differences in identifier names and literal values, iii) Type-3: Syntactically similar code fragments that differ at the statement level. The fragments have statements added, modified and/or removed with respect to each other, and iv) Type-4: Code fragments that are semantically similar in terms of what they do, but possibly different in how they do it. This type of clones may have little or no lexical or syntactic similarity between them. An extreme example of exact semantic similarity that has almost no syntactic similarity, is that of two sort functions, one implementing bubble sort and the other implementing selection sort.
 
Starting at Type-3 and onwards, however, lies a spectrum of clones
that, although still within the reach of automatic clone detection,
are increasingly hard to detect. Reflecting the vastness of this
spectrum, the popular clone benchmark BigCloneBench~\cite{bcb_icsme15}
includes subcategories between Type-3 and Type-4, namely Very Strongly
Type-3, Strongly Type-3, Moderately Type-3, and Weakly Type-3, which
merges with Type-4. Clones that are moderately Type-3 and onward fall in the {\em Twilight Zone} of clone detection. 

As researchers develop new techniques for clone detection, they evaluate and compare the detection performance of their techniques with the other clone detectors. Recall and precision are the two most used metrics to compare clone detection techniques. An ideal clone detection tool should have both high recall and high precision.

\section{Problem Statement}

There are two problems that I have identified: i) most clone detection approaches aim to detect specific types of clones, usually up to Type-3. Very few of them attempt at detecting pure Type-4 clones, since it requires analysis of the actual behavior of code -- a hard problem, in general. The reported precision and recall of existing clone detectors drop dramatically in the Twilight Zone, and ii) the measurement of precision and recall, in general, relies on the existence of labeled datasets. A good labeled dataset provides realistic data and believable labels on all the constituents that should be detected by the analysis tool -- in the case of clone detection, all clone pairs are labeled as such. A labeled dataset allows one to measure how many of the results returned by a certain tool are indeed clones or not (precision), and how many of the true clone pairs are detected by the tool (recall). Publicly available labeled datasets allow direct inter-tool comparisons without the uncertainty that exists when two tools are compared with different datasets. There has been good progress with respect to measuring recall systematically. Based on the BigCloneBench
dataset~\cite{Svajlenko:2014:TBD:2705615.2706134},
BigCloneEval~\cite{7816515} estimates recall automatically by
measuring how many of the labeled clone pairs are included in the
output of a clone detector. However, BigCloneEval stops short of
measuring precision because BigCloneBench does not contain labels for
{\em all} possible clone pairs in it. If a clone detector identifies a clone pair that is not marked as such, only manual inspection can tell whether the pair is a false positive or a true positive. So, while clone detectors that work in Java typically report recall using BigCloneEval, the determination of their precision is still a manual process, leading to difficulties in comparing with other tools.

To this end, I propose to not only push the boundaries of accurate and scalable clone detection to the Twilight Zone, but also to create a system to systematically estimate precision of clone detectors by significantly reducing the human effort involved in such studies.

\section{Approach}

I propose \Name, a scalable method-level clone detector that is
capable of detecting not just Type-1 through strong Type-3 clones, but also clones in the Twilight Zone. The key insights behind the design of \Name\ are twofold: (1)
functionally similar pieces of code tend to do similar {\em actions},
as embodied in the functions they call and the state they access; and
(2) not all pieces of code that do similar actions are functionally
similar; however, it is possible to {\em learn}, by examples, a
combination of metric weights that can predict whether two pieces of
code that do the same actions are clones of each
other. 

For semantic similarity, I use a novel \textit{Action Filter} to filter out a large number of method pairs that don't seem
to be doing the same actions, focusing only on the candidates that
do. For those potential clones, I pass them through a supervised
machine learning model, trained using software metrics, that predicts whether they are clones or not. A deep learning model is trained based on a set of metrics derived from
source code. 

Additionally, in order to systematically evaluate clone detectors, I propose InspectorClone, a web application that enables researchers to systematically estimate precision while significantly reducing the manual effort. To design InspectorClone, I plan to reuse some of the components of \Name. InspectorClone uses the Action Filter and a modified version of supervised machine learning model from \Name\ to automatically resolve many clone pairs during precision studies. This, in turn, reduces the manual effort involved in such studies. While the model training for \Name\ focuses on achieving a good balance between both recall and precision, the trained model for InspectorClone will focus only on high precision to make sure whatever pair InspectorClone predicts as a true clone, should indeed be a true clone.

\section{Thesis} 
\Name, a clone detection tool designed using software metrics, improves the state-of-the-art in code clone detection
by being both the most accurate and the most scalable technique known so far to detect clones in the Twilight Zone. 
%Moreover, InspectorClone, a system built by reusing the components of \Name, significantly reduces the manual effort involved in precision studies of method level clone detectors.

\section{Evaluation}
I compare \Name's detection performance
against the latest versions of the four publicly available clone detection tools, namely: SourcererCC~\cite{sajnani2016sourcerercc}, NiCad~\cite{roy2008nicad}, CloneWorks~\cite{svajlenko2017fast}, and Deckard~\cite{jiang2007deckard}.

I also wanted to include tools such as SeByte~\cite{keivanloo2012sebyte}, Kamino~\cite{neubauer2015kamino}, JSCTracker~\cite{elva2012jsctracker}, Agec~\cite{kamiya2013agec}, and approaches presented in~\cite{white2016deep, ijcai2017-423, Sheneamer:icmla16, tekchandani2013semantic}, which claim to detect Type-4 clones. 
On approaching the authors of these tools, it was communicated that the tool implementation of their techniques currently does not exist and with some authors, I failed to receive a response. Authors of~\cite{gabel2008scalable} and~\cite{jiang2009automatic} said that they do not have implementations for detecting Java clones (They work either on C or C++ clones). I will continue approaching the authors to check the status of their tools.

I demonstrate the scalability of \Name\ in two parts. In the first
part, I show the impact of Action Filter on the number of candidates to be processed. The second part is a run time performance experiment on the IJaDataset~\cite{ijadataset}, a large inter-project Java
repository containing 25,000 open-source projects (3 million source
files, 250MLOC) mined from SourceForge and Google Code. Only 2 other tools (SourcererCC and CloneWorks) have been shown to scale to this dataset and they both detect clones up to Type-3.

\section{Timeline}
- Develop \Name\ (technique and tool): Finished \\
- Evaluate of \Name\ against Open-source tools: Finished \\
- Develop InspectorClone, a webapp to facilitate precision evaluation studies of Clone detectors: Finished\\
- Improve InspectorClone by reusing concepts from \Name\ to automatically resolve Type-2 clones: Mid June, 2018\\
- Improve InspectorClone by reusing concepts from \Name\ to automatically resolve \textit{VST3} clones: Mid August\\
- Write Dissertation: Mid August to End of November, 2018\\
- Dissertation Submission: End of November, 2018\\
- Defense: Mid-December, 2018\\
 

\section{Publications}
\begin{itemize}
	\item V. Saini, F. Farahani, Y. Lu, P. Baldi, and C. Lopes. Oreo: Detection of clones in the twilight zone. Accepted in 26th ACM Joint European Software Engineering Conference and Symposium on the Foundations
	of Software Engineering, FSE 2018. ACM, 2018.
	
	\item V. Saini, D. Yang, F. Farahani, P. Martins, and C. Lopes. Towards Automating Precision Studies of Clone Detectors. Submitted to 2018 IEEE International Conference on Software Maintenance and Evolution
	(ICSME), 2018.
	
	\item C. V. Lopes, P. Maj, P. Martins, V. Saini, D. Yang, J. Zitny, H. Sajnani, and J. Vitek. Déjàvu: A Map of
	Code Duplicates on Github. Proc. ACM Program. Lang., 1(OOPSLA):84:1–84:28, Oct. 2017.
	
	\item D. Yang, P. Martins, V. Saini, and C. Lopes. Stack Overflow in Github: Any Snippets There? In 2017
	IEEE/ACM 14th International Conference on Mining Software Repositories (MSR), pages 280–290, May
	2017.
	
	\item V. Saini, H. Sajnani, and C. Lopes. Cloned and non-cloned Java Methods: A Comparative Study. Empirical
	Software Engineering, Dec 2017.
	
	\item V. Saini, H. Sajnani, and C. Lopes. Comparing Quality Metrics for Cloned and Non Cloned Java Methods: A
	Large Scale Empirical Study. In 2016 IEEE International Conference on Software Maintenance and Evolution
	(ICSME), pages 256–266, Oct 2016.
	
	\item SourcererCC and SourcererCC-I: Tools to Detect Clones in Batch Mode and During Software Development
	V Saini, H Sajnani, J Kim, C Lopes Proceedings of the 38th International Conference on Software Engineering
	(ICSE), 2016 [14]
	
	\item H. Sajnani, V. Saini, J. Svajlenko, C. K. Roy, and C. V. Lopes. Sourcerercc: Scaling Code Clone Detection
	to Big-code. In Proceedings of the 38th International Conference on Software Engineering, ICSE ’16, pages
	1157–1168, New York, NY, USA, 2016. ACM.
	
	\item H. Sajnani, V. Saini, and C. Lopes. A Parallel and Efficient Approach to Large Scale Clone Detection. Journal
	of Software: Evolution and Process, 27(6):402–429, 2015.ware: Evolution and Process, 2015 [19]
	
	\item H. Sajnani, V. Saini, J. Ossher, and C. V. Lopes. Is Popularity a Measure of Quality? An Analysis of
	Maven Components. In 2014 IEEE International Conference on Software Maintenance and Evolution,
	pages 231–240, Sept 2014.
	
	\item H. Sajnani, V. Saini, and C. V. Lopes. A Comparative Study of Bug Patterns in Java Cloned and Non Cloned
	Code. In 2014 IEEE 14th International Working Conference on Source Code Analysis and Manipulation,
	pages 21–30, Sept 2014.
	
	\item V. Saini, H. Sajnani, J. Ossher, and C. V. Lopes. A Dataset for Maven Artifacts and Bug Patterns Found in
	Them. In Proceedings of the 11th Working Conference on Mining Software Repositories, MSR 2014, pages
	416–419, New York, NY, USA, 2014. ACM.
	
\end{itemize} 


\bibliographystyle{abbrv}
\bibliography{bib}
\end{document}