\section{Manual Analysis of Semantic Clones}\label{sec:oreo_qualitative}

During the precision study, we saw pairs which were hard to
classify into a specific class. We also observed some examples where the code
snippets had high syntactic similarity but semantically they were
implementing different functionality and vice-versa.

We saw an interesting pair, in which one method's identifiers were
written in Spanish and the other's in English. These methods offered similar, but not exactly the same, functionality of copying content
of a file into another file. The method written in English iterated on
a collection of files and copied the contents of each file to an
output stream. The method in Spanish copied the content of one file to
an output stream. Action Filter correctly identifies the semantic
similarities in terms of the library calls of these methods, and later
our DNN model correctly recognizes the structural similarity, ignoring the
language differences.

\begin{lstlisting} [label={lst:sort}, float,floatplacement=H,caption=Clone Pair Example: 1] 
private void sortByName() {
	int i, j;
	String v;
	for (i = 0; i < count; i++) {
		ChannelItem ch = chans[i];
		v = ch.getTag();
		j = i;
		while ((j > 0) && (collator.compare(chans[j - 1].getTag(), v) > 0)) {
			chans[j] = chans[j - 1];
			j--;
		}
		chans[j] = ch;
	}
}
----------------------------------------
public void bubblesort(String filenames[]) {
	for (int i = filenames.length - 1; i > 0; i--) {
		for (int j = 0; j < i; j++) {
			String temp;
			if (filenames[j].compareTo(filenames[j + 1]) > 0) {
				temp = filenames[j];
				filenames[j] = filenames[j + 1];
				filenames[j + 1] = temp;
			}
		}
	}
}
\end{lstlisting}
\begin{lstlisting}[label={lst:ext},float,floatplacement=H,caption=Clone Pair Example: 2] 
public static String getExtension(final String filename) {
	if (filename == null || filename.trim().length() == 0 || !filename.contains(".")) return null;
	int pos = filename.lastIndexOf(".");
	return filename.substring(pos + 1);
}
----------------------------------------
private static String getFormatByName(String name) {
	if (name != null) {
		final int j = name.lastIndexOf('.') + 1, k = name.lastIndexOf('/') + 1;
		if (j > k && j < name.length()) return name.substring(j);
	}
	return null;
}
\end{lstlisting}

\begin{lstlisting}[label={lst:fp},float,floatplacement=H,caption=False Positive Example] 
public static String getHexString(byte[] bytes) {
	if (bytes == null) return null;
	StringBuilder hex = new StringBuilder(2 * bytes.length);
	for (byte b : bytes) {
		hex.append(HEX_CHARS[(b & 0xF0) >> 4]).append(HEX_CHARS[(b & 0x0F)]);
	}
	return hex.toString();
}
----------------------------------------
String sequenceUsingFor(int start, int stop) {
	StringBuilder builder = new StringBuilder();
	for (int i = start; i <= stop; i++) {
		if (i > start) builder.append(',');
		builder.append(i);
	}
	return builder.toString();
}
\end{lstlisting}

Here, we present two examples of clone pairs with high semantic similarity and low syntactic similarity.
Listing~\ref{lst:sort} shows one of the classical examples of Type-4 clone pairs reported by \Name. As it can be observed, both of these methods aim to do sorting. The first one implements \textit{Insertion Sort}, and the second one implements \textit{Bubble Sort}. The \AF\ finds many common \AT\ like three instances of \textit{ArrayAccess} action tokens, and 2 instances of \textit{ArrayAccessBinary} action tokens, leading to a high semantic match. Further, the trained model finds high structural match as both models have two loops where one is nested inside another; first method declares three variables whereas the second declares four. \Name\ does not know that both functions are implementing different sorting algorithms, and hence catching a Type-4 clone here can be attributed to chance. Nevertheless, these two implementations share enough semantic and structural similarities to be classified as a clone pair by \Name.

Another example is illustrated in Listing~\ref{lst:ext} where both methods attempt to extract the extension of a file name passed to them. The functionality implemented by both methods is the same, however, the second method does an extra check for the presence of / in its input string (line 9). We were
unsure whether to classify this example as a WT3/T4 or an MT3
since, although some statements are common in both, they are placed
in different positions. Moreover, the syntactic similarity of tokens is also low as both methods are using different variable names.
These examples demonstrate that \Name\ is capable of detecting semantically similar clone pairs that share little syntactical information.

Besides true positives, we found some false positives too. An example is shown in Listing~\ref{lst:fp}. \AF\ captures similar occurrences of \textit{toString()} and \textit{append()} in both
methods and finds a high semantic match. The DNN model also finds
the structures of both of these methods to be similar as both contain a \textit{loop}, an \textit{if statement}, and both declare same number of variables, leading to
the false prediction. Having a list of stop words for \AT\ repeated in many code fragments may help filter out such methods.
