\section{Introduction}
\label{sec:introduction}

This chapter introduces \Name, a scalable method-level clone detector that is
capable of detecting not just Type-1 through strong Type-3 clones, but
also clones in the Twilight Zone. In our experiments, the recall
values for \Name\ are similar to other state of the art tools in
detecting Type-1 to strong Type-3 clones. However, \Name\ performs
much better on clones where the syntactic similarity reduces below
70\% -- the area of clone detection where the vast majority of clone
detectors do not operate. The number of these harder-to-detect clones
detected by \Name\ is one to two orders of magnitude higher than the
other tools. Moreover, \Name\ is scalable to very large datasets.

%% \Name performs equally
%% well for Type-1 through strong Type-3. For clones in the Twilight
%% Zone, \Name has more than twice the recall of the best one for
%% moderate Type-3 clones, and has similar recall as the best one for
%% weak Type-3/Type-4, but more than double the precision.

The key insights behind the development of \Name\ are twofold: (1)
functionally similar pieces of code tend to do similar {\em actions},
as embodied in the functions they call and the state they access; and
(2) not all pieces of code that do similar actions are functionally
similar; however, it is possible to {\em learn}, by examples, a
combination of metric weights that can predict whether two pieces of
code that do the same actions are clones of each
other. For semantic similarity, we use a novel \textit{Action
  Filter} to filter out a large number of method pairs that don't seem
to be doing the same actions, focusing only on the candidates that
do. For those potential clones, we pass them through a supervised
machine learning model that predicts whether they are clones or not. A
deep learning model is trained based on a set of metrics derived from
source code. We demonstrate that \Name\ is accurate and scalable.

%% \begin{figure}
%% 	\centering
%% 	\fbox{\includegraphics[scale=0.6] {Process_outline.pdf}}
%% 	\caption{Overview of the approach.}
%% 	\label{fig:overview}
%% 	\vspace{-0.225in}
%% \end{figure}

The results presented in this paper were obtained by training the metrics
similarity model using SourcererCC~\cite{sajnani2016sourcerercc}, a
state of the art clone detector that has been shown to have fairly
good precision and recall up to Type-3 clones (but not
Type-4). However, our approach is not tied to SourcererCC; any
accurate clone detector can be used to train the model. Specifically,
many clone detection techniques like graph-based or AST-based, which
are accurate but hard to scale, can be used in the training
phase. 
%More generally, \Name\ is designed to incorporate models from an
%ensemble of accurate clone detectors that specialize in certain kinds
%of clones, in order for the accuracy to be even better.
		
%% One approach that tends to have very high recall in detecting up to
%% Type-3 clones is the use of software metrics~\cite{mayrand1996experiment,Kontogiannis:1997fk,patenaude1999extending,keivanloo2012java}, such as number of
%% conditionals, number of statements, number of arguments, etc. Given
%% that only these metrics are looked at, the metrics-based approach is
%% resilient to changes in identifiers and literals, which is at the core
%% of Type-3 cloning. However, the metrics-based approach, by itself,
%% tends to have very poor precision, as it falsely identifies a large
%% number of fragments as clones of each other when they happen to have
%% similar metrics. For that reason, clone detectors that use metrics use
%% additional features to prevent so many false positives. Another
%% problem with using metrics for anything other than 100\% similarity is
%% fine-tuning the thresholds of similarity: given that many metrics are
%% used, there is a large number of configurations between the
%% thresholds of each individual metric, and hence finding the right balance
%% can be hard (e.g. is the number of conditionals more meaningful than
%% the number of arguments?).

%% Because clone detection can be processing-intensive, clone detectors may
%% suffer from scalability problems, limiting the size of the dataset on
%% which they operate. For large datasets, the runtime efficiency of
%% clone detectors is as important as their effectiveness in detecting
%% clones. Hence, features that are easier to extract are
%% preferred over those that require longer processing. Similarly,
%% identifying and eliminating pairs that are not clones early on in the
%% processing pipeline, is much better than propagating those pairs until
%% the end.


%% Moreover, the metrics based approaches are good
%% candidates to detect clones incrementally. With every new input
%% dataset one needs to calculate the metrics for code fragments in this
%% newer dataset and then compare these newer methods to the methods in
%% older and this new dataset. There is no need to reprocess the older
%% dataset.
		

%% There are however a few issues with such approaches: i) metrics-based
%% approaches are prone to false-positives. This happens because metrics
%% comparison ignores the semantics of the code fragments, which leads to
%% the detection of false positives; ii) the rules and thresholds to
%% compare candidate-pairs are defined manually. With increase in the
%% number of metrics, it becomes increasingly difficult to capture the
%% optimum rules and thresholds. iii) candidate-explosion. The number of
%% candidates, for which metrics should be compared before labeling them
%% as clones or non-clones, get very big with the increase in the size of
%% the dataset. A naive approach would compare each method to every other
%% method. A slightly better approach would filter out candidates based
%% on some size-threshold. None of these approaches scale very well to
%% large datasets though; and iv) the candidate-explosion issue leads to
%% the increase in the total runtime of the system, and so metrics-based
%% approaches tend to be slow.
		
%% There are other techniques like hybrid of token and index-based
%% techniques which are shown to be fast, scalable, and highly
%% parallel-able~\cite{}. These techniques need some source-code
%% normalization techniques to be able to detect harder Type-3
%% clones~\cite{}. The detection gets harder when most of the tokens in
%% two methods are different. This is because to validate a clone pair,
%% many tokens in one method need to be found in the candidate clone
%% method.
		
%% Also, researchers have tried combination of metrics and machine
%% learning based techniques to train models to learn the complex rules
%% and thresholds~\cite{}. These techniques are promising but they also
%% suffer from the candidate explosion issue and therefore are hard to
%% scale.
		
%% To take advantage of the desirable qualities of the metric-based,
%% information-retrieval-based and machine learning based approaches we
%% decided to address the above mentioned issues. To this end, we present
%% a novel approach, which is a combination of information-retrieval,
%% metrics, and machine-learning based techniques.
		
		
%		\textbf{Need for Incremental, scalable and accurate clone detection.}
%		
%		\textbf{Existing techniques which are accurate but do not scale}
%		
%		\textbf{Existing techniques which are accurate, shown to scale but are not incremental. Also, the type of clones they will miss.}
%		
%		\textbf{To address the need and inherit the qualities of accurate clone detectors we present our approach which is fast, scalable, incremental and highly parallel-able. }
		
	
		%We note that we are only looking at the quantitative aspect of \textit{Documentation} and are not analyzing the documentation qualitatively.
		
		% Talk about the results here

The contributions of this paper are as follows:
\begin{itemize}
	\item \textbf{Detection of clones in the Twilight Zone}. Compared to
	reported results of other clone detectors in the literature,
	\Name's performance on \HTD\ clones is the best so far.
	\item \textbf{Analysis of clones in the Twilight Zone}. Besides
	quantitative results, we present analysis of examples of
	\HTD\ clones -- a difficult task, even for humans, of deciding
	whether they are clones, and the reasons why \Name\ succeeds
	where other clone detectors fail.
	\item \textbf{Process-pipeline to learn from slow but accurate clone
		detector tools and scale to large datasets}. The clone detection techniques which are accurate but hard to scale can be used to train a model and predict clones in a scalable manner using the concepts introduced in this paper. 
	\item \textbf{Deep Neural network with Siamese architecture}. We
	propose Siamese architecture~\cite{baldi93finger} to detect clone
	pairs. An important characteristic of this architecture is that it can handle the
	symmetry~\cite{Montavon2012} of its input vector (presenting the pair $(a,b)$ to the model will be the
	same as presenting the pair $(b,a)$, a desirable property in clone
	detection).
\end{itemize}
 

%% The most important contribution of this work is the scalable approach
%% used for detecting \HTD\ Type-3 and beyond clones. Even though \Name\ is not the
%% first clone detector capable of tackling this difficult niche of \HTD\
%% clones using a metrics-based component, the approach is scalable to
%% very large datasets. We focus on scalability, because that is
%% important to projects that involve very large source code datasets; a
%% clone detector that does not scale well with the size of input is
%% not very useful in practice. With that in mind, we are listing below the concrete
%% contributions of this paper:

%\Name\ is publicly available at {\url{https://github.com/Mondego/oreo}}. All data used in
%this study is also publicly available, and is submitted as
%supplementary data. 
		
The remainder of this chapter is organized as follows. Section~\ref{sec:oreo_concepts} presents three concepts that are parts of our
proposed approach and are critical to its performance; Section~\ref{sec:oreo_approach} explains the deep neural network model used in our
approach and how it was selected and configured; Section~\ref{sec:oreo_clonedetection} describes the clone detection process using the
concepts introduced in Sections \ref{sec:oreo_concepts} and~\ref{sec:oreo_approach}.

