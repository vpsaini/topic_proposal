\raggedleft Page\\
\contentsline {chapter}{LIST OF FIGURES}{v}{section*.2}
\contentsline {chapter}{LIST OF TABLES}{viii}{section*.4}
\contentsline {chapter}{ACKNOWLEDGMENTS}{x}{section*.6}
\contentsline {chapter}{CURRICULUM VITAE}{xii}{section*.7}
\contentsline {chapter}{ABSTRACT OF THE DISSERTATION}{xvii}{section*.8}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Terminology}{2}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Code Clone Terms}{2}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Code Clone Types}{2}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}The Twilight Zone\ }{3}{subsection.1.1.3}
\contentsline {section}{\numberline {1.2}Motivation}{5}{section.1.2}
\contentsline {section}{\numberline {1.3}Problem Statement}{6}{section.1.3}
\contentsline {section}{\numberline {1.4}Thesis}{8}{section.1.4}
\contentsline {section}{\numberline {1.5}Contributions}{9}{section.1.5}
\contentsline {chapter}{\numberline {2}Background and Related Work}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Existence of Code Cloning}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}Applications of Clone Detection}{12}{section.2.2}
\contentsline {section}{\numberline {2.3}Clone Detection Techniques and Tools}{14}{section.2.3}
\contentsline {section}{\numberline {2.4}Evaluation of Clone Detection Techniques}{17}{section.2.4}
\contentsline {section}{\numberline {2.5}Chapter Summary}{18}{section.2.5}
\contentsline {chapter}{\numberline {3}Empirical Studies}{20}{chapter.3}
\contentsline {section}{\numberline {3.1}Study 1. Do Cloned Methods Differ from Non-cloned Methods?}{21}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Definitions and Thresholds}{23}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Research Questions}{24}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Findings}{25}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Related Work}{26}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Study Design}{30}{subsection.3.1.5}
\contentsline {subsubsection}{\numberline {3.1.5.1}Dataset}{31}{subsubsection.3.1.5.1}
\contentsline {subsubsection}{\numberline {3.1.5.2}Code Cloning}{32}{subsubsection.3.1.5.2}
\contentsline {subsubsection}{\numberline {3.1.5.3}Software Quality Metrics}{36}{subsubsection.3.1.5.3}
\contentsline {subsection}{\numberline {3.1.6}Study Results}{42}{subsection.3.1.6}
\contentsline {subsubsection}{\numberline {3.1.6.1}Dividing each metric by the size metric- NOS}{46}{subsubsection.3.1.6.1}
\contentsline {subsubsection}{\numberline {3.1.6.2}Regression}{50}{subsubsection.3.1.6.2}
\contentsline {subsubsection}{\numberline {3.1.6.3}Methods without Method body}{54}{subsubsection.3.1.6.3}
\contentsline {subsection}{\numberline {3.1.7}Mixed Method Analysis}{59}{subsection.3.1.7}
\contentsline {subsubsection}{\numberline {3.1.7.1}Analysis of Methods}{61}{subsubsection.3.1.7.1}
\contentsline {subsubsection}{\numberline {3.1.7.2}Analysis of Clone Pairs}{76}{subsubsection.3.1.7.2}
\contentsline {subsubsection}{\numberline {3.1.7.3}Analysis of Clone Groups}{82}{subsubsection.3.1.7.3}
\contentsline {subsubsection}{\numberline {3.1.7.4}Summary of Mixed Method Analysis Findings}{86}{subsubsection.3.1.7.4}
\contentsline {subsection}{\numberline {3.1.8}Limitations of this Study}{87}{subsection.3.1.8}
\contentsline {subsection}{\numberline {3.1.9}Conclusion}{89}{subsection.3.1.9}
\contentsline {section}{\numberline {3.2}Study 2. How Much of GitHub is Duplicated?}{90}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Contributions}{91}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Analysis Pipeline}{93}{subsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.1}Tokenization}{93}{subsubsection.3.2.2.1}
\contentsline {subsubsection}{\numberline {3.2.2.2}Database}{94}{subsubsection.3.2.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.3}Project-Level Analysis}{95}{subsubsection.3.2.2.3}
\contentsline {subsubsection}{\numberline {3.2.2.4}SourcererCC}{96}{subsubsection.3.2.2.4}
\contentsline {subsection}{\numberline {3.2.3}Corpus}{97}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Summary of Results}{99}{subsection.3.2.4}
\contentsline {subsubsection}{\numberline {3.2.4.1}File-Level Analysis}{100}{subsubsection.3.2.4.1}
\contentsline {subsubsection}{\numberline {3.2.4.2}File-Level Analysis Excluding Small Files}{101}{subsubsection.3.2.4.2}
\contentsline {subsubsection}{\numberline {3.2.4.3}Inter-Project Analysis}{102}{subsubsection.3.2.4.3}
\contentsline {subsection}{\numberline {3.2.5}SourcererCC Performance Improvements}{103}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}Conclusions}{106}{subsection.3.2.6}
\contentsline {chapter}{\numberline {4}Oreo: Scalable and Accurate Clone Detection in the Twilight Zone}{107}{chapter.4}
\contentsline {section}{\numberline {4.1}Introduction}{108}{section.4.1}
\contentsline {section}{\numberline {4.2}The Oreo\ Clone Detector}{110}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Scalability}{110}{subsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.1.1}Preprocessing}{111}{subsubsection.4.2.1.1}
\contentsline {subsubsection}{\numberline {4.2.1.2}Size Similarity Sharding}{112}{subsubsection.4.2.1.2}
\contentsline {subsubsection}{\numberline {4.2.1.3}Semantic Similarity: The Action Filter}{114}{subsubsection.4.2.1.3}
\contentsline {subsection}{\numberline {4.2.2}Metrics Similarity}{116}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Clone Detection Pipeline}{117}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Learning Metrics}{118}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Dataset Curation}{118}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Deep Learning Model}{119}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Model Selection}{121}{subsection.4.3.3}
\contentsline {section}{\numberline {4.4}Related Work}{124}{section.4.4}
\contentsline {section}{\numberline {4.5}Chapter Summary}{126}{section.4.5}
\contentsline {chapter}{\numberline {5}Evaluation of Oreo}{127}{chapter.5}
\contentsline {section}{\numberline {5.1}Recall}{128}{section.5.1}
\contentsline {section}{\numberline {5.2}Precision}{130}{section.5.2}
\contentsline {section}{\numberline {5.3}Precision in the Twilight Zone}{131}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Classification of Clone Pairs}{136}{subsection.5.3.1}
\contentsline {section}{\numberline {5.4}Scalability}{139}{section.5.4}
\contentsline {section}{\numberline {5.5}Manual Analysis of Semantic Clones}{141}{section.5.5}
\contentsline {section}{\numberline {5.6}Limitations of this Study}{144}{section.5.6}
\contentsline {section}{\numberline {5.7}Chapter Summary}{144}{section.5.7}
\contentsline {chapter}{\numberline {6}InspectorClone: Precision Studies Made Easy}{145}{chapter.6}
\contentsline {section}{\numberline {6.1}Introduction}{145}{section.6.1}
\contentsline {section}{\numberline {6.2}InspectorClone}{148}{section.6.2}
\contentsline {section}{\numberline {6.3}Automatic Classification of Clones}{150}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Overview of the Approach}{151}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Automatic Resolution of Type I\ Clones}{152}{subsection.6.3.2}
\contentsline {subsection}{\numberline {6.3.3}Automatic Resolution of Type II\ Clones}{152}{subsection.6.3.3}
\contentsline {subsection}{\numberline {6.3.4}Automatic Resolution of\nobreakspace {}Type III\ Clones}{154}{subsection.6.3.4}
\contentsline {subsubsection}{\numberline {6.3.4.1}Dataset Curation}{155}{subsubsection.6.3.4.1}
\contentsline {subsubsection}{\numberline {6.3.4.2}Deep Learning Model}{159}{subsubsection.6.3.4.2}
\contentsline {subsubsection}{\numberline {6.3.4.3}Sensitivity Analysis}{162}{subsubsection.6.3.4.3}
\contentsline {section}{\numberline {6.4}Evaluation}{163}{section.6.4}
\contentsline {section}{\numberline {6.5}Related Work}{168}{section.6.5}
\contentsline {section}{\numberline {6.6}Threats to Validity and Limitations}{170}{section.6.6}
\contentsline {section}{\numberline {6.7}Chapter Summary}{171}{section.6.7}
\contentsline {chapter}{\numberline {7}Conclusion}{172}{chapter.7}
\contentsline {section}{\numberline {7.1}Dissertation Summary}{172}{section.7.1}
\contentsline {section}{\numberline {7.2}Lessons Learned}{174}{section.7.2}
\contentsline {section}{\numberline {7.3}Future Work in Clone Detection}{177}{section.7.3}
\contentsline {chapter}{Bibliography}{179}{section*.86}
\contentsline {chapter}{Appendices}{192}{appendix*.88}
\contentsline {section}{\numberline {A}Experience Report on Using Software Metrics for Clone Detection}{193}{section.G.1}
\contentsline {subsection}{\numberline {A.1}Train and Test Data Creation}{195}{subsection.G.1.1}
\contentsline {subsubsection}{\numberline {A.1.1}Model Selection}{197}{subsubsection.G.1.1.1}
\contentsline {subsubsection}{\numberline {A.1.2}Evaluation}{198}{subsubsection.G.1.1.2}
\contentsline {section}{\numberline {B}Running Oreo}{202}{section.G.2}
\contentsline {subsection}{\numberline {B.1}Prerequisites}{202}{subsection.G.2.1}
\contentsline {subsubsection}{\numberline {B.1.1}System Requirements}{202}{subsubsection.G.2.1.1}
\contentsline {subsubsection}{\numberline {B.1.2}Required Dependencies}{202}{subsubsection.G.2.1.2}
\contentsline {subsection}{\numberline {B.2}Generate Input for Oreo.}{203}{subsection.G.2.2}
\contentsline {subsection}{\numberline {B.3}Setting Up Oreo}{204}{subsection.G.2.3}
\contentsline {subsection}{\numberline {B.4}Running Oreo}{205}{subsection.G.2.4}
\contentsline {subsection}{\numberline {B.5}Quick Execution of Oreo}{206}{subsection.G.2.5}
