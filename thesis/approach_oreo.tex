\section{Learning Metrics}\label{sec:oreo_approach}

For anything other than minor deviations from equality, the use of
software metrics for clone detection leads to an explosion of
configuration options. To address this issue, we use a supervised
machine learning approach. The trained model learns the best
configuration of the $24$ software metrics from a training set of clones and
non-clones. In this section, we describe the dataset used to
train the model, and also, the trained model and its selection process.

\begin{comment}

\begin{figure}[b]
\fbox{\includegraphics[scale=0.6] {Train.pdf}}
\caption{Model Train Process}
\label{fig:train}
\end{figure}
\subsubsection{Model Training Stage} \label{sec:train_process}
\end{comment}

\subsection{Dataset Curation}\label{sec:dataset}

To prepare a dataset, we download 50k random Java projects from GitHub. We then extract methods with 50 or more tokens from
these projects; this ensures we do not have empty methods in the
dataset. Also, it is the standard minimum clone size for
benchmarking~\cite{bcb_icsme15}. To get \textit{isClone} labels, we used
SourcererCC, a state of the art Type-3 clone detector. From this
dataset, we randomly sample a labeled dataset of 50M feature vectors,
where 25M vectors correspond to clone pairs and other 25M to non clone
pairs. Each feature vector has the \textit{isClone} label and 48 metrics (24 for each method).

%However, our approach is not tied to SourcererCC; any state of the art clone detector can be used to generate the clone labels.
\begin{comment}
We parsed these methods to obtain the 24 metrics discussed in Section \ref{sec:metrics} for each of them.
%In the end we had a dataset of \textbf{THIS MANY} methods. 
%677,177
%Finally, we generated clone-pairs for these methods using SourcererCC.

In order to train a classification model, we needed a dataset consisting of method pairs, and an \textit{is\_clone} label for each pair showing whether this pair forms a clone pair or not. Deriving the list of method pairs and their \textit{is\_clone} labels can be done manually or by utilizing a clone detector tool. Since, detecting
the cloned method pairs manually is a time consuming process for large
datasets; a faster way is to use a clone detector. We used the
clone pairs reported by SourcererCC \cite{sajnani2016sourcerercc}, a type-3 clone detector, for this purpose. We used SourcererCC because it has been shown to have a high recall in type-1,type-2, and type-3 clone types \cite{sajnani2016sourcerercc}. However, our approach is not tied with a specific clone detector, and depending on needs, any tool can
be used for this purpose. To produce the dataset usable in model training, 24 metrics derived for each method were matched with SourcererCC output to produce a feature vector of a method pair, is\_clone label, and 24 software metrics for each method (a total of 48 metrics in each row).
The final dataset used in model selection and training consisted of 50 million randomly sampled method pairs out of the whole 50k Java projects. We made this dataset balanced so that half of the pairs correspond to clones, and half do not. 
\end{comment}
For model selection purposes, we randomly divide the dataset into $80\%$ pairs for training, and $20\%$ pairs for testing. 
One million pairs from the training set are kept aside for validation and hyper-parameter tuning purposes. It should be noted that to avoid any significant favorable bias in our experiments, we do not use BigCloneBench's dataset during training. 


%It should be noted that we do not use BigCloneBench's dataset~\cite{svajlenko2016bigcloneeval} for training, as this dataset is used for benchmarking purpose only. Training and testing on the benchmarking dataset will induce a significant favorable bias, and we avoid that by creating a fresh dataset for training.
\subsection{Deep Learning Model}\label{sec:deeplearn}
\textit{The model training and model selection were carried out by Prof. Pierre Baldi and his Ph.D. student, Yadong Lu. The dataset used for model training and selection was curated by Farima Farmahinifarahani, a fellow Ph.D. student, and myself.}

While there exists many machine learning techniques, here we are using deep learning to detect clone pairs.
Neural networks, or deep learning methods are among the most prominent machine learning methods that utilize multiple layers of neurons (units) in a network to achieve automatic learning. Each unit applies a nonlinear transformation to its inputs. These methods provide effective solutions due to their powerful feature learning ability and universal approximation properties. 
%Deep architectures of artificial neural networks have the capability to learn complex representations hierarchically from raw data. Through model training, deep architectures are able to automatically select discriminative representations that are useful for making accurate predictions in subsequent classification stages. 
Along with scaling well to large datasets, these approaches can take advantage of well maintained software libraries and can also compute on clusters of CPUs, GPUs and on the cloud.  %Comparing to other machine learning methods such as support vector machine \cite{Cortes1995}, random forest \cite{Breiman2001} and logistic regression which have many applications on small datasets, deep neural network models are more frequently used where a large dataset is availble. 
Deep Neural Networks (DNN) have been successfully applied to many areas of science and technology \cite{schmidhuber2015deep}, such as computer vision \cite{NIPS2012_4824}, natural language processing \cite{socher2012deep}, and even biology \cite{deepcontact2012}. 

\begin{comment}
The use of deep learning for the purpose of clone detection has been proposed by Davey et al. ~\cite{davey1995development} and later revisited by White et al. \cite{white2016deep}. Their approach is an unsupervised technique which learns from source code directly. Whereas, \Name\ uses a supervised learning approach to learn from a set of source code metrics.
\end{comment}
Here we propose to use a Siamese architecture neural network \cite{baldi93finger} to detect clone pairs. Siamese architectures are best suited for problems where two objects must be compared in order to assess their similarity, for example comparing fingerprints~\cite{baldi93finger}.
%An example of such problems is comparing fingerprints
Another important characteristic of this architecture is that it can handle the symmetry~\cite{Montavon2012} 
of its input vector. Which means, presenting the pair $(m1,m2)$ to the model will be the same as presenting the pair $(m2,m1)$.
%This ability is achieved by applying the same operation to each component of the pair by using two identical sub neural networks. 
%This is crucial in clone detection, the equality of clone pair $(m1,m2)$ with $(m2,m1)$ is an issue that should be addressed while detecting or reporting clone pairs. 
The other benefit brought by Siamese architectures is a reduction in the number of parameters; the weight parameters are shared within two identical sub neural networks making it require fewer number of parameters than a plain architecture with the same number of layers. 

\begin{figure}%[t]
	\centering
	\fbox{\includegraphics[width=.95\linewidth]{nn_new.pdf}}
	\caption{Siamese Architecture Model}
	\label{fig:oreo_Siamese}
	\vspace{-0.2in}
\end{figure}

Figure~\ref{fig:oreo_Siamese} shows the Siamese architecture model trained for \Name. Here, the input to the model is a 48 dimensional vector created using the 24 metrics described in Section~\ref{sec:metrics}. 
%The first 24 components correspond to the metric values of the first method, and the second 24 components correspond to the metric values of the second method. 
This input vector is split into two input instances corresponding to two feature vectors associated with two methods. The two identical subnetworks then apply the same transformations on both of these input vectors. 
%These two subnetworks have the same configuration and always share the same parameter values while the parameters are getting updated. 
Both have 4 hidden layers of size 200, with full connectivity (each neuron's output in layer n-1 is the input to neurons in layer n). 

The outputs of the two subnetworks are
then added and fed to the comparator network which has four
layers of sizes 200-100-50-25, with full connectivity between the
layers. The output of this comparator network is then fed to the Classification Unit which consists of a logistic unit mathematically represented as $f(\sum_{i=1}^{25}w_{i}\cdot x_{i}) = \frac{1}{1+e^{-\sum_{i=1}^{25}w_{i}\cdot x_{i}}}$. Where, $x_{i}$ is the i-th input of the final classification unit, and $w_{i}$ is the weight parameter corresponding to $x_{i}$. The product $w_{i} \cdot x_{i}$, is summed over $i$ ranging from 1 to 25 since we have 25 units in Layer 8 (the layer before Classification unit). The output of this unit is a value between 0 and 1, and can be interpreted as the probability of the input pair being a clone. We claim that a clone pair is detected if this value is above 0.5.
%\begin{equation}\label{eq:logistic}
%f(\sum_{i=1}^{25}w_{i}\cdot x_{i}) = \frac{1}{1+e^{-\sum_{i=1}^{25}w_{i}\cdot %x_{i}}}
%\end{equation} All the neurons in the layers use ReLU activation function
($O^{n}_{i} = max(I^{n}_{i}, 0)$, where $O^{n}_{i}$, $I^{n}_{i}$ are
respectively the output and input of the i-th neuron in layer n)
\cite{Glorot+al-AI-2011} to produce their output. 
%or to predict chemical reactions by comparing electron source-sink pairs \cite{kayala2011learning,
%baldiRP2017}.

% \iffalse
% We propose a deep neural network  model to automatically learn the features of code pairs and dectect the clone pair. This model can potentially utilize its deep architecture to extract latent information on correlations of the features from each pair. Its strong representational learning ability \cite{6472238} enables the model to identify a clone code pair based on the similarities inherent in the features. 
% \fi

 %e because it does not share weight paramters within the network. 


% \iffalse
% such that it is forced to learn the same representation even
% %ouputs exactly the same probablity value of the pair being clone even
% if we switch the order of the first and the second 24 features.
% \fi 
% % Other approaches, such as random forests, do not always capture this symmetry.
% \iffalse
%  However, models such as random forest does not have this invariance property if the order of the input features is changed. 
%  \fi
% \iffalse
% We achieve this invariance  by designing a two paths neural network with tied weight and merge them together in higher layers. As shown in figure 1, we seperate each training example into two 24 dimensional vectors and feed them into two paths which share the same weight parameters. Then we merge them together and add dense layers on top in order to further extract the information and disguish clone pairs from normal pairs. It is shown in our result that this structure achieves better performance than plain model with dense layers. And it also results $34.7\%$ reduction in number of parameters comparing with the model where we don't tied the weight. As a result, it significantly reduces the amount of time and computations to train the model. 
% \fi
In this model, to prevent overfitting, a regularization
technique called dropout \cite{baldidropout14} is applied to every
other layer. In this technique, during training, a proportion of the
neurons in a layer is randomly dropped along with their connections
with other neurons. In our experiment, we achieve the best performance
with $20\%$ dropout. The loss function (function that quantifies the difference between
generated output and the correct label)
used to penalize the incorrect classification is the relative entropy
\cite{kullback1951} between the distributions of the predicted output
values and the binary target values for each training
example. Relative entropy is commonly used to quantify the distance
between two distributions.  Training is carried out by stochastic
gradient descent with the learning rate of 0.0001.  The learning rate
is reduced by $3\%$ after each training step (epoch), to improve the
convergence of learning. The parameters are initialized randomly using
\lq he normal\rq \cite{He}, a common initialization technique
in deep learning. Training is done in minibatches where the
parameters are updated after training on each minibatch. Since the
training set is large, we use a relative large minibatch size of
1,000.
\subsection{Model Selection}

\begin{figure*}
	\begin{minipage}[b]{0.45\linewidth}
		\includegraphics[width=1\linewidth]{train_acc_font1.pdf} 
		\caption{Training Accuracy} 
		\label{fig:trainacc}
	\end{minipage}
	\begin{minipage}[b]{0.45\linewidth}
		\includegraphics[width=1\linewidth]{valid_acc_font1.pdf} 
		\caption{Validation Accuracy } 
		\label{fig:valacc}
	\end{minipage}
	\begin{minipage}[b]{0.45\linewidth}
		\includegraphics[width=1\linewidth]{train_loss_font1.pdf} 
		\caption{Training Loss} 
		\label{fig:trainloss}
	\end{minipage}
	\begin{minipage}[b]{0.45\linewidth}
		\includegraphics[width=1\linewidth]{valid_loss_font1.pdf} 
		\caption{Validation Loss} 
		\label{fig:valloss}
	\end{minipage}
	\begin{minipage}[b]{0.45\linewidth}
		\includegraphics[width=1\linewidth]{AUC_font1.pdf} 
		\caption{ROC Plot and AUC Values} 
		\label{fig:ROC}
	\end{minipage}
	%\caption{Top left: Training Accuracy; Top right: Validation Accuracy; Bottom left: Training Loss; Bottom right: Validation Loss. } 
	\label{fig:FiveFig}
	%\vspace{-0.2in}
\end{figure*}

\begin{table}
	\centering
%	\scriptsize
	\caption{Precision/Recall on Test Dataset}\label{tab:prerecallmodels}
	%\resizebox{6cm}{!}{
	\begin{tabular}{lll}
	%	\tiny
		Model & Precision & Recall \\
		\midrule 
		Logistic Regression & 0.846    & 0.886 \\
		Shallow NN          & 0.931    & 0.963 \\
		Random forest       & 0.93      & 0.94 \\
		Plain DNN           & 0.939    & 0.972 \\
		Siamese DNN        & 0.958    & 0.974
	\end{tabular}
	%		}
%	\vspace{-0.2in}
\end{table}

To find the model, we experiment
with several architectures, for each architecture, several number of layers and units, and several hyper-parameter settings such as
learning rate (the rate for updating the weight parameters) and loss
function. To compare these models, we compute several
classification metrics including accuracy (the rate of correct
predictions based on validation dataset labels), Precision (the
fraction of retrieved instances that are relevant), Recall (the
fraction of relevant instances that are retrieved), Receiver Operating
Characteristic (ROC) curve (true positive rate against false positive
rate), and Area Under the ROC Curve (AUC). The selection process is described in the rest
of this section. 

As mentioned, in the process of selecting the best model, we also train other models based on different architectures
including: (1) A simple logistic regression model, (2) A shallow
neural network (Shallow NN) model with a single hidden layer and
similar amount of parameters as in Siamese model, and (3) a plain
fully connected network (Plain DNN) with the same layer sizes as the
full Siamese architecture. For each architecture, we train many models; and for the sake of simplicity, here we compare the best model from each mentioned architecture. All models are trained on the same
dataset for 50 epochs and training is terminated if the validation
loss stops increasing for two consecutive epochs.

Results comparing the best model from each mentioned architecture are
reported in Figure \ref{fig:trainacc} to Figure \ref{fig:ROC}, as well
as in Table \ref{tab:prerecallmodels}.  The Siamese network model
outperforms all other models in every metric.  Figure
\ref{fig:trainacc} illustrates the accuracy attained by each model
through the epochs in Training, and Figure \ref{fig:valacc} shows the
same concept in Validation.  The Siamese Deep Neural Network and
Plain DNN have better accuracy than other two models. However, the
Siamese DNN, designed to accommodate the symmetry property of its
overall input, outperforms the accuracy of the Plain DNN. More
importantly, this model is performing better than the Plain DNN on the
validation set, despite using significantly less free
parameters. Thus, the Siamese architecture is considered to have
better generalization properties on new samples. Figure
\ref{fig:trainloss} depicts the decrease in training loss over
the epochs for each model, and Figure \ref{fig:valloss} shows the same
concept for validation loss. In Figure \ref{fig:trainloss}, we observe
that the training loss for logistic regression and shallow NN models
stops improving at around 0.8. Whereas, the loss for plain NN and
Siamese DNN can go below 0.09 as we train longer. A similar pattern is
observed for validation loss in Figure \ref{fig:valloss}. The large
fluctuations for shallow NN are due to the small size of the
validation set.

%since the validation set is relatively small, so we observe fluctuation in figure 5.%Similar patterns are oberseved in validation loss. 
%YADONG: IN THE FIGURES YOU NEED TO CHANGE THE NAMES OF THE MODELS TO BE CONSISTENT WITH WHAT I WROTE--SOMETHING LIKE:
% Logistic Regression
% Shallow NN
% Plain DNN
% Siamese DNN
%PLEASE INTRODUCE THE NN AND DNN ACRONYMS IN THE TEXT OR IN THE FIGURE LEGENDS
\begin{comment}
\begin{figure}%[H]
	\centering
	% \parbox{8cm}{
	\includegraphics[width=8cm,height=5cm]{AUC.pdf}
	
	\caption{ Receiver Operating Characteristic (ROC) Plot and Area Under the Curve (AUC) Values for Different Models on Validation Dataset}
%	\label{fig:ROC}
	\vspace{-0.2in}
\end{figure}
\end{comment}
Figure~\ref{fig:ROC} shows the ROC curves of the different classifiers
and compares the corresponding AUC values for validation
dataset. A high AUC value denotes a high
true positive rate and low false negative rate. As shown in Figure
\ref{fig:ROC}, the Siamese architecture leads to the best AUC value
(0.995). Finally, Precision and Recall performances on test dataset
are compared in Table \ref{tab:prerecallmodels}. The table shows that
the Siamese DNN has a recall comparable with the Plain DNN, but has a
better precision (0.958 vs 0.939). Totally, Siamese DNN outperforms
other models in both precision and recall values.
 
Other than the mentioned differences, compared to the plain network,
the Siamese architecture model has around 25,000 parameters which is 37\%
less than the plain structure, which leads to less training time and
less computation burden.