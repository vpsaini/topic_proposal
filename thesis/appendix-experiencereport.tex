\section{Experience Report on Using Software Metrics for Clone Detection}

Metric-based approaches are not new to detect clones in software systems. Ottenstein in 1976 used Halstead complexity measures to detect plagiarism in software~\cite{Ottenstein:1976:AAD:382222.382462}. Since then other researchers have also proposed metrics based approaches to detect similar code in software~\cite{Donaldson:1981:PDS:800037.800955, mayrand1996experiment, Kontogiannis:1997fk}. Software metrics based approaches show good promise if th goal is to detect very similar code clones, which usually fall into \tone\ and \ttwo\ clone categories. The intuition is that if two code snippets are copy-paste clones, then the metrics computed on these two snippets should be very similar. Therefore, instead of comparing tokens of the two snippets, one can compare these metrics values and identify clones if the absolute difference is less than or equal to the delta
threshold defined for each of the corresponding metrics. 
However, as the structural differences between the snippets start to increase, for example in \tthree\ clones, it becomes difficult to manually configure these delta values for each of the metrics used in clone detection technique. Moreover, different combinations of these delta values are needed to be considered to identify clones with good accuracy, a configuration explosion problem. It becomes very difficult to humanly analyze all different configuration which makes it difficult to use metric based approaches to detect clones with high accuracy. Moreover, metric based approaches also suffer from candidate explosion problem which makes it very difficult for them to scale large datasets. During my work on using software metrics for clone detection I also faced these issues first hand. 

My work on comparing cloned methods to non cloned methods~\cite{saini2016comparing} seeded the idea to use software metrics for clone detection. I along with Farima, a fellow Ph.D. student, started exploring the idea of using software metrics to reduce the comparisons between the code snippets. We tried an approach where we calculated some simple metrics for every code snippets. These metrics include \textit{number of language tokens}, \textit{number of unique language tokens}, \textit{number of characters}, and \textit{number of operators}. We wanted to retrieve the possible clone candidates of a query method in constant time. to this end, we calculated the above metrics for every method in our dataset. Then, we sorted these methods based on the number of tokens in these methods. To get all candidate clones of a query method, we did two binary searches on this sorted dataset to find the upper and lower index of the methods such that the number of tokens in these methods are similar to that of the query method. The idea behind this approach was that two methods in a clone pair should have similar number of tokens. Using this approach, we were successful in filtering out a large number of methods without performing any comparisons between them and the query method. Now, the next challenge was to compare the query method with all candidate methods to identify clone pairs. To this end, we explored with comparing the remaining metrics of every candidate pair. Soon we realized that for each metric comparison we need to manually set the threshold values. We did not know what should be these thresholds. For example, should we say that a pair is a clone pair if their number of tokens are with in 80\% and number of unique tokens are within 90\% and so on. To get this threshold values we decided to manually look at the clone pairs and their corresponding metric values. After multiple iterations of manually setting the threshold values and computing clone detection using our approach, we learned that our approach is not good in terms of precision. 

To overcome the configuration explosion challenge, we decided to use machine learning. To this end, we used the dataset of 3,562 Java projects which I created in one of my previous work ~\cite{saini2016comparing}. The
dataset consists of 3,562 Java projects hosted on
Maven~\cite{Maven}. The comprehensive list of projects with their
version information is available
at~\url{http://mondego.ics.uci.edu/projects/clone-metrics/}.

\textbf{Dataset Artifacts and Properties:}
We used following artifacts from the dataset described above: 

\textit{1) Clone-pairs}. Intra-project method-level clone detection
results for 3,562 projects. The clone detection was carried out using
SourcererCC. Overall, 412,705 cloned and 616,604 non-cloned methods were identified by SourcererCC in this dataset.

\textit{2) Method-level metrics}. This artifact consists of
method-level metrics calculated for the methods in this dataset. The metrics were calculated at the using
version 6 of the JHawk tool ~\cite{urlJHawk}. JHawk has been widely
used in academic studies on Java metrics
~\cite{gupta2005fuzzy,alghamdi2005oometer,benestad2006assessing,arisholm2006predicting,arisholm2007data}. Table~\ref{tab:conc_metric-breakdown}
shows the $25$ metrics for which the computed values were available
in the artifact. Many of these metrics are standard metrics. A set of
metrics are derived from the Software Quality Observatory for Open
Source Software (SQO-OSS)~\cite{Samoladas:2008pb}. SQO-OSS is
composed of well-established and validated software quality metrics,
which can be computed either from source code or from surrounding
community data. More details on these metrics and the dataset can be found
elsewhere~\cite{saini2016comparing}.

\label{sec:quality-metrics}
\begin{table}[!tbp]
	\centering
	\caption{Software Quality Metrics}
%	\begin{center}
	%	\resizebox{8cm}{!}{
			\begin{tabular} {l l l}
				\thickhline
				\hlx{v}
				Name & Description \\
				\hlx{vhv}
				\hlx{vhv}
				XMET & External methods called by the method\\
				VREF & Number of variables referenced\\
				VDEC & Number of variables declared\\
				TDN & Total depth of nesting\\
				NOS & Number of statements\\
				NOPR & Total number of operators\\
				NOA & Number of arguments\\
				NLOC & Number of lines of code\\
				NEXP & Number of expressions\\
				NAND & Total number of operands\\
				MOD & Number of modifiers\\
				MDN & Method, Maximum depth of nesting\\
				LOOP & Number of loops (for,while)\\
				LMET & Local methods called by the method\\
				HVOL & Halstead volume\\
				HVOC & Halstead vocabulary of method\\
				HLTH & Halstead length of method\\
				HEFF & Halstead effor to implement a method\\
				HDIF & Halstead difficulty to implement a method\\
				HBUG & Halstead prediction of number of bugs\\
				EXCT & Number of exceptions thrown by the method\\
				EXCR & Number of exceptions referenced by the method\\
				CREF & Number of classes referenced\\
				COMP & McCabes cyclomatic complexity\\
				CAST & Number of class casts\\
				
				\thickhline
			\end{tabular}
	%	}
		
		\label{tab:conc_metric-breakdown}
%	\end{center}
%	\vspace{-0.245in}
\end{table}

\subsection{Train and Test Data Creation} \label{sec:feas_traintestcreate}
We separated the whole dataset at hand randomly to two parts: 60\% for \textit{train} dataset, and the rest \textit{test}. We produced features for both datasets, and for train dataset, we integrated each Feature Vector with its corresponding \textit{is\_clone} label based on SourcererCC.

To have a manageable sized dataset, we filtered out pairs for which the percentage difference in NOS is more than 30\%. Having the dataset ready, the first step to train the target clone detector model was to select a classification model which fits our needs and has a satisfactory performance both in terms of accuracy and timing. This process is explained in Section~\ref{sec:feas_modelselec}. The trained model was then tested on the reserved unseen test data; results of this process is explained in Section~\ref{sec:feas_evaluation}. 

\subsubsection{Model Selection} \label{sec:feas_modelselec}
To select a target model, we performed 10-fold cross validation, (a common form of N-fold cross validation ~\cite{refaeilzadeh2009cross}), with various algorithms. However, since our training dataset was 45GB in size, training multiple models using 10-fold cross validation(CV) was not feasible in terms of timing. Hence, we selected three different random samples from this dataset: a sample with 10 thousand rows, a sample with 50 thousand rows, and another with 100 thousand rows. Having three different samples could help us get assured that the model selection process is general and independent of characteristics of a single dataset sample. Each of the three sampled datasets were split into 70\% train and 30\% test.
%Each of the three sampled datasets were divided to two parts: a 70\% portion named training, and a 30\% named testing. 
10-fold cross validation was performed on the training dataset, and then each model was trained on the whole training dataset and tested on testing dataset. The purpose was to reserve an unseen portion in each sample and compare the results of 10-fold cross validation with results on an unseen bunch of data. The results of this 10-fold cross validation is given in the Table~\ref{tab:10CV}. 
%This 
%that could further assure us as to the generalization of our results.


We ran our experiments with multiple classifiers: K-Nearest Neighbors (KNN)~\cite{wu2008top}, Naive Bayes~\cite{wu2008top} , Classification and Regression Trees (CART)~\cite{wu2008top}, Logistic Regression (LR)~\cite{dreiseitl2002logistic}, Linear Discriminant Analysis (LDA)~\cite{izenman2008modern}. For these models, we measured the Precision and Recall (with respect to SourcererCC) , and also, the time each of them takes.
Performance of each classifier after being trained on 70\% training dataset and validating on the 30\% unseen testing data is given in Table~\ref{tab:test7030}. In this table, T\textsubscript{L} denotes time taken for learning a model, T\textsubscript{P} stands for time taken to predict clones, and time measurements are in seconds. Precision and Recall numbers are measured with respect to the reference clone detector (SourcererCC). 
%Results of 10-fold cross validation are not included because of space limitations.

\begin{sidewaystable}[htb]
	\caption{Results of 10-fold Cross Validation on Sampled Rows from Train Dataset}
	\label{tab:10CV}
	\bigskip
	\centering
	%\small
	\setlength\tabcolsep{2pt}
	\hspace*{-1cm}\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|c}
		\cline{1-13}
		Dataset       & \multicolumn{4}{l|}{10 Thousand Rows} & \multicolumn{4}{l|}{50 Thousand Rows} & \multicolumn{4}{l|}{100 Thousand Rows} &  \\ \cline{1-13}
		\backslashbox {Model}{Result} & Prec.   & Recall  & F1    & Time  & Prec.   & Recall  & F1    & Time  & Prec.   & Recall  & F1    & Time   &  \\ \cline{1-13}
		KNN (K=5)     & 92\%        & 92\%    & 92\%  & 1.6   & 92\%        & 96\%    & 94\%  & 33.6  & 93\%        & 97\%    & 95\%  & 145.3  &  \\ \cline{1-13}
		KNN (K=10)    & 91\%        & 94\%    & 93\%  & 1.5   & 92\%        & 97\%    & 94\%  & 30.3  & 93.4\%      & 97\%    & 95\%  & 135.3  &  \\ \cline{1-13}
		Naive Bayes   & 89\%        & 93\%    & 90\%  & 0.2   & 90\%        & 92\%    & 91\%  & 1.5   & 90\%        & 92\%    & 91\%  & 2.6   &  \\ \cline{1-13}
		CART          & 89\%        & 95\%    & 91\%  & 0.6   & 92\%        & 96\%    & 94\%  & 3.2   & 93\%        & 97\%    & 95\%  & 7.2   &  \\ \cline{1-13}
		SVM           & 96\%        & 89\%    & 93\%  & 31.7  & 96\%        & 93\%    & 94\%  & 1632  & 95\%  & 95\%  & 95\% &  12771      &  \\ \cline{1-13}
		LR            & 92\%        & 94\%    & 93\%  & 1.1   & 91\%        & 93\%    & 92\%  & 7.4   & 92\%        & 93\%    & 92\%  & 18.1   &  \\ \cline{1-13}
		LDA           & 87\%        & 94\%    & 90\%  & 0.3   & 88\%        & 94\%    & 91\%  & 1.9   & 88\%        & 94\%    & 91\%  & 4.6    &  \\ \cline{1-13}
		Random Forest & 93\%        & 94\%    & 94\%  & 0.5   & 94\%        & 96\%    & 95\%  & 2.7   & 95\%        & 97\%    & 96\%  & 5.7    &  \\ \cline{1-13}
		AdaBoost     & 93\%        & 93\%   & 94\%  & 2.9   & 94\%        & 96\%    & 95\%  & 11.7  & 95\%        & 97\%    & 96\%  & 24.4   &  \\ \cline{1-13}
	\end{tabular}\hspace*{-1cm}
\end{sidewaystable}



\begin{sidewaystable}[htb]
	\caption{Results of Training Models and Validating Them on Unseen Portion of Sampled Rows from Train Dataset}
	\label{tab:test7030}
	\bigskip
	\centering
	%\small
	\setlength\tabcolsep{2pt}
	\hspace*{-1cm}\begin{tabular}{l|ccccc|ccccc|ccccc}
		%\cline{1-13}
		\hline
		Dataset       & \multicolumn{5}{l|}{10 Thousand Rows}                 & \multicolumn{5}{l|}{50 Thousand Rows} &  \multicolumn{5}{l}{100 Thousand Rows}    \\ \hline
		\backslashbox {Model}{Result} & Prec. & Recall & F1   & T\textsubscript{L} & T\textsubscript{P} & Prec. & Recall & F1   & T\textsubscript{L} & T\textsubscript{P} & Prec. & Recall & F1   & T\textsubscript{L} & T\textsubscript{P} \\ \hline
		KNN     & 90\%      & 92\%   & 91\% & 0.03       & 0.7          & 93\%      & 97\%   & 95\% & 0.2       & 21           & 94\%      & 97\%   & 95\% & 0.7       & 68.7         \\ \hline
		%KNN (K=10)    & 91\%      & 94\%   & 93\% & 0.02       & 0.7          & 93\%      & 98\%   & 95\% & 0.2       & 17           & 94\%      & 97\%   & 96\% & 0.8       & 58.4         \\ \hline
		Naive Bayes   & 86\%      & 92\%   & 89\% & 0.02       & 0.01         & 92\%      & 92\%   & 92\% & 0.1       & 0.05         & 90\%      & 91\%   & 91\% & 0.2       & 0.08         \\ \hline
		CART          & 88\%      & 95\%   & 91\% & 0.1        & 0.008        & 92\%      & 97\%   & 94\% & 0.3       & 0.03         & 93\%      & 96\%   & 95\% & 0.7       & 0.05         \\ \hline
		SVM           & 93\%      & 89\%   & 91\% & 3.6        & 1.4          & 97\%      & 93\%   & 95\% & 211       & 35.4         & 96\%      & 94\%   & 95\% & 2079      & 143          \\ \hline
		LR            & 89\%      & 93\%   & 91\% & 0.1        & 0.005        & 92\%      & 93\%   & 93\% & 0.7       & 0.03         & 91\%      & 92\%   & 92\% & 1.6       & 0.05         \\ \hline
		LDA           & 87\%      & 94\%   & 90\% & 0.03       & 0.01         & 89\%      & 94\%   & 92\% & 0.2       & 0.04         & 87\%      & 93\%   & 90\% & 0.3       & 0.06         \\ \hline
		Random Forest & 91\%      & 93\%   & 92\% & 0.04       & 0.01         & 94\%      & 96\%   & 95\% & 0.2       & 0.03         & 95\%      & 96\%   & 95\% & 0.5       & 0.09         \\ \hline
		AdaBoost     & 89\%      & 94\%   & 91\% & 0.2        & 0.01         & 95\%      & 96\%   & 95\% & 0.9       & 0.04         & 94\%      & 96\%   & 95\% & 2.9       & 0.09         \\ \hline
	\end{tabular}\hspace*{-1cm}
\end{sidewaystable}


%At the first step, we tried single classifiers, and noted down their performance.
Based on the experiments we selected CART as it had better accuracy and speed than others.
%Experiments revealed that KNN, CART and SVM, had a better accuracy than others;  but, KNN and SVM were much more slower than CART. Thus, we selected CART as our target classifier. 
However, classification models are prone to get biased and over fitted to train data and not to generalize to unseen data well. As a result, we tested 
%an ensemble of classifiers based on the selected algorithm (CART): We tested %AdaBoost ~\cite{wu2008top} and 
Random Forest~\cite{ho1995random}, 
%both of these algorithms build an ensemble of classifiers: 
which builds several Decision Trees and takes the majority vote among them as the final vote. 
%and AdaBoost builds several models of a configured base model, and combines them ~\cite{wu2008top}. 
As it was expected, Random Forest improved accuracy, while taking more time than CART. However, the increase in time was negligible. Hence, Random Forest was selected as the final model. 
The next step was to select an optimal configuration for Random Forest. To this end, we decided to run experiments on our sampled datasets and then proceed with a select set of configurations to the whole train dataset. After doing 10-fold cross validation, and training on 70\% of data and testing on 30\% of it, using multiple parameters on the sampled datasets, we found 5 configurations that were performing better than others. Results for these shortlisted configurations on the 100 thousand rows dataset are given in Table~\ref{tab:shortlist100k}.

Having shortlisted 5 classifiers, we selected the configuration presented in first row as the best one. The decision was made on the basis of F1 score and prediction time taken by each model. We then proceeded to train this model on the whole train dataset and predict using it on the unseen 40\% reserved portion of data. 

\begin{sidewaystable}[htb]
	\caption{Performance of Random Forest Classifier on 100 Thousand Rows Sample}
	\label{tab:shortlist100k}
	\bigskip
	\centering
	%\small
	\setlength\tabcolsep{2pt}
	\hspace*{-1cm}
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|l|}
		\hline
		\multirow{2}{*}{\#Est.} & \multirow{2}{*}{Max D} & \multirow{2}{*}{Min S} & \multirow{2}{*}{Min L} & \multirow{2}{*}{Max F} & \multicolumn{4}{c|}{10- Fold Cross Validation} & \multicolumn{5}{c|}{30\% Unseen Data (2-Fold Cross Validation)} \\ \cline{6-14} 
		&                        &                        &                        &                        & Prec.      & Recall      & F1       & Time     & Prec.      & Recall      & F1        & t          & t           \\ \hline
		5                       & 20                     & 10                     & 5                      & Sqrt(\#Features)       & 95\%       & 96\%        & 95\%     & 5.6      & 94\%       & 96\%        & 95\%      & 0.43 s     & 0.06 s      \\ \hline
		5                       & --                     & --                     & --                     & --                     & 95\%       & 97\%        & 96\%     & 5.7      & 95\%       & 96\%        & 95\%      & 0.49 s     & 0.09 s      \\ \hline
		10                      & 10                     & 20                     & 5                      & Sqrt(\#Features)       & 94\%       & 95\%        & 95\%     & 8.1      & 95\%       & 95\%        & 95\%      & 0.82s      & 0.084 s     \\ \hline
		50                      & 5                      & --                     & --                     & Sqrt(\#Features)       & 95\%       & 86\%        & 90\%     & 26.1     & 95\%       & 85\%        & 90\%      & 2.81 s     & 0.18 s      \\ \hline
		25                      & 10                     & --                     & --                     & Sqrt(\#Features)       & 95\%       & 96\%        & 96\%     & 17.5     & 95\%       & 95\%        & 95\%      & 1.83 s     & 0.126 s     \\ \hline
	\end{tabular}\hspace*{-1cm}
\end{sidewaystable}


\subsubsection{Evaluation} \label{sec:feas_evaluation}
To measure the performance of the selected model on unseen data, we decided to  train a model on the whole train dataset. 
%However, the whole
%training dataset had 173,766,592 rows, and training on all of these
%rows were not feasible. 
However, after analyzing the this dataset, we observed that there exists approximately 7 times more cloned pairs than non-cloned pairs. Hence, we did an inclusive sampling over the data in which equal number of clones and non-clones were present. This ensured us that the model is being trained correctly and accurately.
%observed that there were 22,151,199 clone pairs, and 151,615,393
%non-clone pairs present in the train dataset. We decided to have an
%inclusive and manageable sampling over the train dataset: A sample in
%which beside including all clone pairs, 22,151,199 random non-clone
%pairs were selected; this yielded to a dataset that included equal
%number of clones and non-clones. This dataset could also assure us
%that the model is being trained correctly and accurately as it sees
%equal number of cloned and non-cloned methods. 
Testing the trained model on the 40\%
unseen test data, resulted in 88\% Precision and 97\% Recall with respect to SourcererCC's results. Model train took 570 seconds and Prediction took 345 seconds for the whole
test dataset which were promising numbers. These numbers gave us the confidence in using machine learning to address the problem of configuration explosion.
