\subsection{Definitions}
\label{sect:defs}
In this section, we elaborate on the terms and definitions that are pivotal to discussing \IC's mechanisms.

\textbf{Clone Pair}: A pair of code fragments that are similar,
specified by the triple (f1, f2, $\phi$), including the similar code
fragments f1 and f2, and their clone type $\phi$~\cite{Svajlenko:2015:ECD:2881297.2881379}.

\textbf{Clone Types}: Based on the literature~\cite{roy2009comparison}, our work uses the following
four types of source code clones, the first three being similar on the
textual and syntactic level, and the fourth type defining similarity
on the functional, semantic level:

\textbf{\tone}: Identical code fragments, except
	for differences in white-space, layout and comments.
	
\textbf{\ttwo}: Identical code
	fragments, except for differences in identifier names and literal
	values, as well as \tone\ differences.
	
\textbf{\tthree}: Syntactically similar code
	fragments that differ at the statement level. The fragments have
	statements added, modified and/or removed with respect to each
	other, in addition to \tone\ and \ttwo\ clone differences
	
\textbf{\tfour}: Syntactically dissimilar code fragments that implement the same functionality. 
	
The definition to classify clones as \tthree\ does not specify what should be the minimum syntactical similarity between the methods of a clone pair to be classified as \tthree. Also, the lack of consensus in the community of clone researchers about this similarity makes it difficult to separate \tfour\ and \tthree\ clones. To address this issue, the popular clone benchmark, BigCloneBench~\cite{Svajlenko:2014:TBD:2705615.2706134,
	Svajlenko:2015:ECD:2881297.2881379}, has divided the zone between \tthree\ and \tfour\ into four subcategories based on syntactical similarity values: Very Strongly~\tthree\ (VST3) with similarity in
range of [0.9, 1.0), Strongly~\tthree\ (ST3) with similarity being in [0.7,0.9),
Moderately~\tthree\ (MT3) with similarity in range of [0.5, 0.7), and Weakly~\tthree\ (WT3/4) having similarity in the range of [0.0,0.5). More details about these subcategories can be found
elsewhere~\cite{Svajlenko:2014:TBD:2705615.2706134}.

\textbf{Action Token}: Action tokens of a
method are the tokens corresponding to the methods called and class fields
accessed by that method~\cite{oreopreprint}. Additionally, the array accesses made by a method are also special Action tokens namely \textit{ArrayAccess} and \textit{ArrayAccessBinary}, where array access of kind \textit{arr[i]} is an \textit{ArrayAccess} Action token and \textit{arr[i+1]} is an \textit{ArrayAccessBinary} Action token. In the code provided in Listing~\ref{lst:actiontokens}, Action tokens are: \textit{children()}, \textit{hasMoreElements()}, \textit{nextElement()}, \textit{isFiltered()}, \textit{addElement()}, and \textit{elements()}.

\textbf{Action Filter}: A filter which ensures a minimum amount of similarity between the Action tokens of two methods~\cite{oreopreprint}. 

We use overlap-similarity, calculated as $Sim(A_1,A_2)= |A_1\cap A_2|$, to measure the similarity between the
Action tokens of two methods. Here, $A_1$ and $A_2$ are sets of
Action Tokens in methods $M_1$ and $M_2$, respectively. Each element in
these sets is defined as $<t,freq>$, where $t$ is the Action Token and
$freq$ is the number of appearances of this token in the method. $M_1$ and $M_2$ satisfy the Action filter if $ \frac{Sim(A_1,A_2)}{ max(|A_1|,|A_2|)}$$\geq$$\theta$, where $\theta$ is Action filter threshold such that $0 \leq \theta \leq 1$.

\begin{lstlisting} [label={lst:actiontokens}, float,floatplacement=H,caption=Example: Action Tokens] 
public Enumeration children() {
	Enumeration allChildren = super.children();
	Vector filtered = new Vector();
	DiligentNode node;
	while (allChildren.hasMoreElements()) {
		node = (DiligentNode) allChildren.nextElement();
		if (!node.isFiltered(true)) filtered.addElement(node);
	}
	return filtered.elements();
}
\end{lstlisting}