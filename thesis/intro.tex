\chapter{Introduction}
\label{chapter:thesis}
Source code clone detection is the task of finding similar software pieces, according to a certain concept of similarity. These pieces can be statements, blocks of code, functions, classes, or even complete source files, and their similarity can be syntactic, semantic or both. Over the past 20 years, clone detection has been the focus of increasing attention, with many clone detectors having been proposed and implemented (see~\cite{Sheneamer:survey16} for a recent survey on this topic). These clone detection techniques and tools differ from each other depending on the goals and granularity of the detection. Modern use cases of clone detection~\cite{Lopes:2017:DMC:3152284.3133908} require clone detectors to not only be accurate but also scalable.
%To be able to detect clones in large repositories, clone detectors not only have to be accurate but also are required to be scalable. 
%Moreover, these techniques can be language specific or language agnostic with varying degrees of specialization.
%The scalable and accurate detection of clones is still an active area of research. 
%The popularity of online services like Github and Bigbucket
Recently, researchers have proposed and demonstrated techniques that are accurate and can scale to large datasets~\cite{sajnani2016sourcerercc, svajlenko2017cloneworks}. These techniques however fall short of detecting complex clones where the syntactic similarity between the code snippets is low. The accurate detection of such complex clones in a scalable manner is still an active area of research.
 %While there are many tools and techniques published to detect clones, not much effort is spent on streamlining the evaluation of these tools and techniques. 

%In the following paragraphs, we explain why evaluating clone detection tools is still a challenge. 





\section{Terminology}
%There are four broad categories of clone detection approaches, ranging from easy-to-detect to \HTD\ clones: textual similarity, lexical similarity, syntactic similarity, and semantic similarity.
In this section, I elaborate the following well-accepted terms and definitions that are pivotal in my dissertation ~\cite{bellon2007comparison, bellon,roy:queens:07, Cordy:2011:ICPC, Svajlenko:2015:ECD:2881297.2881379}.
 
\subsection{Code Clone Terms}
\textbf{Code Fragment or Block:} A continuous segment of source code specified by the triple (l,s,e), including the source file, l, the start line of the fragment, s, and the end line, e.


\textbf{Clone Pair:} A pair of code fragments that are similar,
specified by the triple (f1, f2, $\phi$), including the similar code
fragments f1 and f2, and their clone type $\phi$.


\textbf{Near-miss Clone:} Code fragments that have minor to significant editing differences between them. 

\subsection{Code Clone Types}
 In the literature, four commonly accepted clone types can be found:

\begin{itemize}
	\item \tone\ (textual similarity): Identical code fragments, except
	for differences in white-space, layout and comments.
	\item \ttwo\ (lexical, or token-based, similarity): Identical code
	fragments, except for differences in identifier names and literal
	values, in addition to Type-1 clone differences.
	\item \tthree\ (syntactic similarity): Syntactically similar code
	fragments that differ at the statement level. The fragments have
	statements added, modified and/or removed with respect to each
	other, in addition to Type-1 and Type-2 clone differences
	\item \tfour\ (semantic similarity): Syntactically dissimilar code fragments that implement the same functionality. 
	%Code fragments that are semantically similar in terms of what they do, but possibly different in how they do it. 
	%This type of clones may have little or
	%no lexical or syntactic similarity between them. An extreme example
	%of exact semantic similarity that has almost no syntactic
	%similarity, is that of two sort functions, one implementing bubble
	%sort and the other implementing selection sort.
\end{itemize} 

%% The various types of clones are defined in terms of similarity
%% metrics, which means that clone detectors may be looking for exact
%% matches (100\% similarity on the metric of choice) or relaxed matches
%% that pass a certain threshold of similarity. In any case, an ideal
%% clone detector would be able to detect all of these types of clones,
%% even if specific usages would focus only on some of them at a time. 

Clone detectors use a variety of signals from the code (text, tokens,
syntactic features, program dependency graphs, etc.)  and tend to aim
for detecting specific types of clones, usually up to \tthree. Very few
of them attempt at detecting pure Type-4 clones, since it requires
analysis of the actual behavior -- a hard problem, in general.


\subsection{The \TWZ\ }

Starting at \tthree\ and onwards lies a spectrum of clones
that, although still within the reach of automatic clone detection,
are increasingly hard to detect. Reflecting the vastness of this
spectrum, the popular clone benchmark BigCloneBench~\cite{svajlenko2016bigcloneeval}
includes subcategories between \tthree\ and \tfour, namely Very Strongly
\tthree, Strongly \tthree, Moderately \tthree, and Weakly \tthree, which
merges with \tfour.


\begin{lstlisting}[label={lst:mtdTwil},float,caption=Sequence Between Two Numbers] 
// Original method
String sequence(int start, int stop) {
	StringBuilder builder = new StringBuilder();
	int i = start;
	while (i <= stop) {
		if (i > start) builder.append(',');
		builder.append(i);
		i++;
	}
	return builder.toString();
}

// Type-2 clone
String sequence(int begin, int end) {
	StringBuilder builder = new StringBuilder();
	int n = begin;
	while (n <= end) {
		if (n > begin) builder.append(',');
		builder.append(n);
		n++;
	}
	return builder.toString();
}

// Very strongly Type-3 clone
String sequence(short start, short stop) {
	StringBuilder builder = new StringBuilder();
	for (short i = start; i <= stop; i++) {
		if (i > start) builder.append(',');
		builder.append(i);
	}
	return builder.toString();
}

// Moderately Type-3 clone
String seq(int start, int stop){
	String sep = ",";
	String result = Integer.toString(start);
	for (int i = start + 1; ; i++) {
		if (i > stop)	break;
		result = String.join(sep, result, Integer.toString(i));
	}
	return result;
}

// Weakly Type-3 clone
String seq(int begin, int end, String sep){
	String result = Integer.toString(begin);
	for (int n = begin + 1; ;n++) {
		if (end < n)	break;
		result = String.join(sep, result, Integer.toString(n));
	}
	return result;
}

// Type-4 clone
String range(short n, short m){
	if (n == m)
	return Short.toString(n);
	return Short.toString(n)+ "," + range(n+1, m);
}
\end{lstlisting}

In order to illustrate the spectrum of clone detection, and its
challenges, Listing~\ref{lst:mtdTwil} shows one example method followed by
several clones of it, from Type-2 to Type-4. The original method takes two numbers and
returns a comma-separated sequence of integers in between the two
numbers, as a string. The Type-2 clone (starting in line \#13) is
syntactically identical, and differs only in the identifiers used
(e.g. {\texttt begin} instead of {\texttt start}). It is very easy
for clone detectors to identify this type of clones. The very strong
Type-3 clone (starting in line \#25) has some lexical as well as
syntactic differences, namely the use of a for-loop instead of a
while-loop. Altough harder than Type-2, this subcategory of Type-3 is
still relatively easy to detect. The moderate Type-3 clone (starting
in line \#35) differs even more from the original method: the name of
the method is different ({\texttt seq} vs. {\texttt sequence}), the
comma is placed in its own local variable, and the type String is used
instead of StringBuilder. This subcategory of Type-3 clones is much
harder to detect than the previous ones. The weak Type-3 clone
(starting in line\#46) differs from the original method by a
combination of lexical, syntactic and semantic changes: String
vs. StringBuilder, a conditional whose logic has changed ($<$ vs
$>$), and it takes one additional input parameter that allows for
different separators. The similarities here are weak and very hard to
detect. Finally, the Type-4 clone (starting in line \#56) implements
similar (but not the exact same) functionality in a completely
different manner (through recursion), and it has almost no lexical or
syntactic similarities to the original method. Detecting Type-4
clones, in general, requires a deep understanding of the intent of a
piece of code, especially because the goal of clone detection is
similarity, and not exact equivalence (including for semantics). Clones that are moderately Type-3 and onward fall in the {\em Twilight
	Zone} of clone detection. 

\section{Motivation}
The advent of web-hosted open source repository services such as GitHub, {\sf BitBucket} and {\sf SourceForge} have transformed how source code is
shared. Over the last two decades, millions of projects have been shared, building up a massive trove of free software. The availability of such a large corpus of source code has attracted clone researchers to mine it in the hopes of finding patterns of interest. 
Such large corpora present the opportunity to improve the \SOA\ in various modern use cases of clone detection. Some of these use cases include studying the extent of cloning within and across code hosting repository platforms~\cite{lopes:oopsla17, Yang:2017:SOG:3104188.3104224}, studying patterns of clone evaluation~\cite{beck:2010wc,kim:2005}, conducting empirical studies to understand the characteristics of clones~\cite{saini2016comparing}, detecting similar mobile applications~\cite{chen2014achieving}, license violation detection~\cite{5069483, koschke:2006ast}, mining library candidates~\cite{6385134}, reverse engineering product lines~\cite{5069483, hemel:2012wcre}, code search~\cite{kawaguchi2009shinobi}, and finding the provenance of a component~\cite{julius:2011:msr}.
Though these opportunities are exciting, scaling such vast corpora poses critical challenge making scalability a must-have requirement of modern clone detection tools. This need for scalability in clone detection tools can also be found in literature ~\cite{sajnani2016sourcerercc, lopes:oopsla17, Kim2006ProgramEM,Koschke:CSMR:12}. Quoting Koschke, ``\textit{Detecting license violations of source code requires to compare a suspected system against a very large corpus of source code, for instance, the Debian source distribution. Thus, code clone detection techniques must scale in terms of resources needed.}"    


As mentioned earlier, clones that are moderately \tthree\ and onward fall in the {\em \TWZ} of clone detection. Existing studies show that in the software systems, the \tthree\ clones outnumber the clones of other types~\cite{roy2010near, Svajlenko:2014:TBD:2705615.2706134}. This makes the detection of \tthree\ clones even more important. 
Further, in our experience we have found that there exist more clone pairs in the \TWZ\ than in the simpler \tthree\ subcategories, making it desirable to push the boundaries of clone detection to cover the \TWZ. 



%Existing scalable clone detectors 
\section{Problem Statement}

Many tools and techniques have been published to detect source code clones. For example, in 2013, in a systematic literature review, Rattan et al. found at least 70 clone detection tools and techniques~\cite{RATTAN20131165}. Since then, more tools and techniques, which have improved the \SOA\ have been built ~\cite{sajnani2016sourcerercc, Saini:2016:SST:2889160.2889165, svajlenko2017cloneworks}. While some of these modern tools detect clones up to \tthree\ in a scalable manner, they fall short of detecting clones in the \TWZ. 

Clone detection in general requires comparison among every code fragment to detect all possible clones in a given dataset. This makes clone detection bear a prohibitive $O(n^2)$ time complexity. With the increase in size of datasets, the number of pairs that need to be compared increases quadratically: a candidate explosion problem, posing serious scalability challenges. 

A general strategy for scaling clone detection is to eliminate unlikely clone pairs upfront i.e., before making them undergo heavy computational operations. As mentioned earlier, it becomes harder to detect a clone pair as we move from \tone\ to \tfour\ because the computations get more expensive. Other than the increase in computation load, the number of pairs that need to be compared also increases drastically. 

There exist token based scalable techniques that use aggressive heuristics to filter out pairs which are usually beyond \textit{ST3} category~\cite{sajnani2016sourcerercc, svajlenko2017cloneworks}. This enables them to scale large datasets at the cost of poor detection performance in the \TWZ.


These scalable token based techniques use bag-of-words model to detect near-miss \tthree\ clones. The code fragments in \tthree\ clones are created by adding, removing, or modifying statements in a duplicated code fragment. The bag-of-tokens model is resilient to such changes because it is agnostic to the relative positions of tokens in code fragments. This model has been shown to detect near-miss clones as long as the code fragments share enough tokens to exceed a given threshold. The similarity in tokens of the clone pairs in the \TWZ\ drops significantly making such token based techniques ineffective in detecting clones in the \TWZ.

Metrics based approaches for clone detection are known to work very well if the goal is to find only \tone\ and \ttwo\
clones~\cite{mayrand1996experiment, patenaude1999extending,
	Kontogiannis:1997fk}. This is understandable: given the strict definitions of \tone\ and \ttwo, the metrics values of such clone pairs should be mostly the same. For \tthree, metrics might look like a good choice too because metrics are resilient to changes in identifiers and literals. However, the use of metrics for clones in the \TWZ\ is not straightforward, because these clones may be syntactically different. As such, the use of metrics requires fine tuning over a large number of configurations between the thresholds of each individual metric leading to an explosion of configuration options. Finding the right balance manually can be hard, for example, is \textit{the number of conditionals} more meaningful than the \textit{number of arguments}? Apart from the configuration explosion problem, metric based techniques have poor precision in \tthree\ category and also suffer from the candidate explosion problem. 
 %Software metrics based techniques are also promising also shown Another set of techniques which is not only resilient to token positions but is also resilient to changes in the identifier names and literal values is software metric based clone detection.

 Accurate and scalable detection of code clones in the \TWZ\ remains a difficult problem to address.

\begin{comment}
\section{Research Questions}
\textbf{Research Question 1.[Scalability]} - How can we address the problem of candidate explosion in clone detection?

\textbf{Research Question 2.[Recall and Precision]} - How can we be more robust to modifications in cloned codes to detect clones in the \TWZ? 

Research Question 1 focuses on improving scalability of the clone detection technique, whereas Research Question 2 focuses on recall and precision of the clone detection technique.
\end{comment}

\section{Thesis}

The above problems motivate the need for clone detection techniques which satisfy the following requirements: (1) Accurate detection of clones in the \TWZ; (2) Scalability to large datasets of source code without the necessity of special hardware. 
To address the above requirements, I propose \Name, a novel approach to source code clone detection, that not only detects \tone\ to \tthree\ clones accurately, but is also capable of detecting clones in the \TWZ. \Name\ is built using a combination of machine learning, information retrieval and software metrics. \Name\ uses a novel filtering heuristic to address the problem of candidate explosion. This heuristic selects pairs where the semantic similarity between the code fragments is high. Moreover, to identify the structural similarity between the code fragments of these pairs, \Name\ uses a deep neural network model trained using software metrics.\Name\ currently supports method level clone detection in Java.

I can now state my thesis as follows:
\textit{\Name\ improves the \SOA\ in code clone detection by being the most scalable and the most effective technique known so far to detect clones in the \TWZ.}

I compare \Name's\ detection performance against the latest versions of five publicly available clone detection tools, namely, SourcererCC~\cite{sajnani2016sourcerercc}, NiCad~\cite{roy2008nicad}, CloneWorks~\cite{svajlenko2017cloneworks}, Deckard~\cite{jiang2007deckard}, and CCAligner~\cite{Wang:2018:CTB:3180155.3180179}. These tools are considered \SOA\ in the literature as well as in recent clone detection benchmarking experiments. They are also examples of modern clone detection tools that support \tthree\ clone detection. 
%I also wanted to include tools such as SeByte~\cite{keivanloo2012sebyte}, Kamino~\cite{neubauer2015kamino}, JSCTracker~\cite{elva2012jsctracker}, Agec~\cite{kamiya2013agec}, and approaches presented in literature~\cite{white2016deep, ijcai2017-423, Sheneamer:icmla16, tekchandani2013semantic}, which claim to detect \tfour\ clones. On approaching the authors of these tools, we were communicated that the tool implementation of their techniques, currently does not exist and with some authors, we failed to receive a response. Authors of~\cite{gabel2008scalable} and~\cite{jiang2009automatic} said that they do not have implementations for detecting Java clones (They work either on C or C++ clones).
  
To measure recall and precision, I use a popular benchmark of real clones, Big-CloneBench~\cite{Svajlenko:2015:ECD:2881297.2881379}. To measure scalability, I use IJaDataset-2.0, a dataset of large inter-project software repositories consisting of 25,000 projects containing 3 million files and 250 MLOC.

\section{Contributions}
This dissertation makes the following contributions:

(i) Empirical study to understand if cloned methods are different from non-cloned methods. The study explores the relationship between code clones and 27 software quality metrics on a dataset of 3,562 Java projects.(Chapter 3: Study1)

 
(ii) Large scale quantitative and qualitative study to explore the extent of cloning in GitHub, a popular source code repository hosting service. We conducted the study for four popular languages, Java, C/C++, Python, and Javascript. In total, the study analyzes 427,807,061 files across 4,494,438 projects. The study posed challenges related to scalability and execution time of SourcererCC, a \SOA\ clone detector which we used to compute clone detection. This work describes these scalability challenges and how I addressed them. (Chapter 3: Study 2) 


(iii) \Name, a scalable and effective clone detector capable of detecting clones in the \TWZ. To design \Name, I use a subset of software metrics used in the empirical study as referred above in contribution (i). This study addresses scalability and accuracy challenges, the two major issues which software metric based clone detection approaches usually suffer from. To scale to large datasets, \Name\ uses the concepts which I developed while addressing scalability issues in SourcererCC. (Chapter 4)


(iv) Experiments to evaluate scalability and effectiveness (recall and precision) against existing \SOA\ tools. The results of these experiments can be used as benchmarks to compare and evaluate future clone detection tools and techniques. (Chapter 5)


(v) InspectorClone, a web application which facilitates in conducting precision experiments of clone detection tools. InspectorClone significantly reduces the manual effort involved in such precision studies. InspectorClone contributes to the clone research community by making an ongoing dataset of manually tagged clone pairs available publicly for research purposes. (Chapter 6)
