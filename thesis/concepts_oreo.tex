\section{The \Name\ Clone Detector}\label{sec:oreo_concepts}

\begin{figure}
\centering
\fbox{
  \includegraphics[width=.95\linewidth] {Process_outline.pdf}
}
\caption{Overview of \Name.}
\label{fig:oreo_overview}
%\vspace{-0.1in}
\end{figure}

The goals driving the design of \Name\ are twofold: (1) we want to be
able to detect clones in the Twilight Zone without hurting precision,
and (2) we want to be able to process very large datasets consisting
of hundreds of millions of methods. In order to accomplish the first
goal, we introduce the concept of semantic signature, which is based on actions
performed by that method, followed by an analysis of the methods'
software metrics. In order to accomplish the second goal, we first use
a simple size-based heuristic that eliminates a large number of
unlikely clone pairs. Additionally, the use of semantic signatures
also allows us to eliminate unlikely clone pairs early on, leaving the
metrics analysis to only the most likely clone pairs.
Figure~\ref{fig:oreo_overview} gives an overview of \Name.

%% \begin{comment}
%% In order to tackle the issues faced in the reproduction study, three
%% concepts are introduced. The first is \textit{Feature Engineering} that
%% forms the features in a way that they are proper input values to
%% train and prediction processes. This step, explained in Section
%% \ref{sec:feature-eng}, addresses the first issue explained in Section
%% \ref{sec:feas_lessons}. In order to tackle two problems of candidate
%% explosion, and lack of semantic features, we introduce a novel filter
%% named \textit{Action Filter} that captures the similarity of two
%% methods in terms of the fine-grained actions they carry out to reach
%% their final goal. This filter is elaborated in Section
%% \ref{sec:action-filter}. Another concept is \textit{Input
%%   Partitioning} with the aim of further reducing the number of
%% candidates and mitigating candidate explosion problem; this concept is
%% described in Section \ref{sec:indexpar}.

 
%% \subsubsection{Feature Engineering} \label{sec:feature-eng}
%% %Features play a pivotal role in our approach as they are used in training models and in predicting the clone pairs; hence, Feature Engineering is an important step. 
%% We did Feature Engineering in two parts, as follows. First, we conducted feature selection, and then we scaled the features so that they convey useful information and do not confuse the model. 

%% \textbf{Feature Selection}. For many algorithms in machine learning it is advised to remove the features which are highly correlated~\cite{hall1999correlation}. Random-Forrest however, doesn't get affected much by correlated features. %The decision-trees used in Random-forest, during training, calculate the information gain for each feature at every step and then learn to make a decision. The highly correlated features, at a given step, will have a very similar information gain and therefore only one of them will be used to make a decision on that step. This makes having correlated features redundant yet not affecting the model negatively. 
%% As a result, we did not have to analyze the correlations among features heavily. However, we found some interesting patterns in features that needed attention: We noticed that NLOC and NOS values for a very similar code could be different. This is because NLOC captures the number of lines of code, whereas NOS captures the number of statements in code. For example, one could write three statements in one line, decreasing the NLOC value without affecting the number of statements, which will still be three. Thus, NLOC would not have useful information gain for our model; hence, we removed NLOC and reserved NOS. We also removed MOD, which captures the number of modifiers in a method. It is a binary feature (0 or 1) and we noticed that it has no correlation with \textit{is\_clone} label. There is still more scope for feature selection but we decided to go ahead with the remaining features. In future we would do a thorough analyses to reduce as many features as possible. 

%% \textbf{Feature Scaling}. We noticed in the reproduction study that Percentage Difference between the metric values is a good feature but there were many false negatives in our predictions. Analyses showed that percentage difference could mislead our model while training; let's see this with the help of an example. Consider a candidate pair $M_1,M_2$ where $M_1$ has $n_1$ loops and $M_2$ has $n_2$ number of loops such that $n_1>n_2$. The percentage difference between their number of loops, given by~\ref{perdiff}, will be.
%% \begin{equation}
%% \dfrac{n_1-n_2}{n_1}*100
%% \end{equation}
%% The percentage difference does not scale well when we increase $n_1$ and $n_2$. If $n_1$ is 2 and $n_1$ is 1, the percentage difference is 50. However, if $n_1$ is 10 and $n_2$ is 9 the percentage difference is 10. Notice in both cases the absolute difference is 1. Also, if $n_2$ is 0, then for any natural number value of $n_1$, the percentage difference will be 100\%; so there is a definite need of feature scaling. We addressed this by using a very simple technique: We added a constant c to both $n_1$ and $n_2$, where c is a natural number. For most of our metrics which needed scaling, their metric values were in range of 0 to 15. Hence, a very big value of c would have removed important information from the features and a small value like 1 would not scale the features. Considering this and assessing different numbers, we decided to set c to 11. The result of this was to use the formula shown in~\ref{newperdiff} for Feature Engineering.

%% \begin{equation}\label{newperdiff}
%% Percentage-diff(f1,f2)=\dfrac{|f1-f2|}{Max(f1,f2)+11}*100
%% \end{equation}
%% \end{comment}

\subsection{Scalability}\label{sec:oreo_scalability}

The performance improvement we did for SourcererCC, as explained in Section~\ref{sec:PerfImpSourcererCC} forms the basis of making \Name\ a scalable clone detector. To design \Name\ we use the size-based input partitioning which allowed us to create in-memory indexes for faster lookups. In this section I first explain the preprocessing step which allowed us to carry out faster processing during clone detection. Then, I explain how \Name\ uses size similarity sharding and in-memory indexes to achieve high scalability.
 

\subsubsection{Preprocessing}

One key strategy to scaling super-linear analysis of large datasets is
to preprocess the data as much as possible. Preprocessing consists of
a one-time, linear scan of the data with the goal of extracting
features from it that allow us to better organize and optimize the
actual data processing. In \Name, during preprocessing, we extract
%preprocess all the files for extraction of 
several pieces of information about the methods, namely:
(1) their semantic signature (Section~\ref{sec:action-filter}), and
(2) assorted software metrics.

Table~\ref{tab:oreo_jhawk-metrics} shows the $24$ method level metrics
extracted from the source files. A subset of these metrics is derived
from the Software Quality Observatory for Open Source Software
(SQO-OSS)~\cite{Samoladas:2008pb}. The decision of, which SQO-OSS
metric to include is based on one simple condition: a metric's
correlation with the other metrics should not be higher than
a certain threshold. This was done because two highly correlated
metrics will convey very similar information, making the presence of
one of them redundant. From a pair of two correlated metrics, we
retain the metric that is faster to calculate.

Additionally to SQO-OSS, we extract a few more metrics that carry
important information. During our initial exploration of clones in the
Twilight Zone, we noticed many clone pairs where both methods are
using the same type of literals even though the literals themselves
are different. For example, there are many cases where both the
methods are using either \textit{Boolean} literals, or \textit{String}
literals. Capturing the \textit{types} of these literals is important
as they contain information that can be used to differentiate methods
that operate on different types -- a signal that they may be
implementing different functionality. As a result, we add a
set of metrics (marked with $*$ in the Table~\ref{tab:oreo_jhawk-metrics})
that captures the information on how many times each type of literal is
used in a method. A description of many of these software metrics is given in Chapter~\ref{chapter:empirical_studies}.
\begin{comment}
\begin{table}[!tbp]
	\scriptsize
	\begin{center}
		\caption{Method-Level Software Metrics}
		\label{tab:jhawk-metrics}
		\resizebox{6cm}{!}{
			\begin{tabular} {l l l}
				\thickhline
				\hlx{v}
				Name & Description \\
				\hlx{vhv}
				\hlx{vhv}
				XMET & Number of external methods called\\
				VREF & Number of variables referenced\\
				VDEC & Number of variables declared\\
		%		TDN$*$ & Total depth of nesting\\
				NOS & Number of statements\\
				NOPR & Total number of operators\\
				NOA & Number of arguments\\
		%		NLOC$*$ & Number of lines of code\\
				NEXP & Number of expressions\\
				NAND & Total number of operands\\
		%		MOD$*$ & Number of modifiers\\
				MDN & Method, Maximum depth of nesting\\
				LOOP & Number of loops (for,while)\\
				LMET & Number of local methods called\\
		%		HVOL$*$ & Halstead volume\\
				HVOC & Halstead vocabulary\\
	%			HLTH$*$ & Halstead length of method\\
				HEFF & Halstead effort to implement\\
				HDIF & Halstead difficulty to implement\\
		%		HBUG$*$ & Halstead prediction of number of bugs\\
				EXCT & Number of exceptions thrown\\
				EXCR & Number of exceptions referenced\\
				CREF & Number of classes referenced\\
				COMP & McCabes cyclomatic complexity\\
				CAST & Number of class casts\\
		%		NOC$*$ & Number of Comments\\
		%		NOCL$*$ & Number of Commented Lines\\
				NBLTRL$*$ & Number of Boolean Literals\\
				NCLTRL$*$ & Number of Character Literals\\
				NSLTRL$*$ & Number of String Literals\\
				NNLTRL$*$ & Number of Numerical literals\\
				NNULLTRL$*$ & Number of Null Literals\\
				\thickhline
			\end{tabular}
		}		
	\end{center}
	\vspace{-0.2in}
\end{table}
\end{comment}

\begin{table}[!tbp]
	%\tiny
	\centering
	%\begin{center}
		\caption{Method-Level Software Metrics}
		\label{tab:oreo_jhawk-metrics}
	%	\resizebox{\linewidth}{!}{
			\begin{tabular} {l l l l}
				\thickhline
				\hlx{v}
				Name & Description &Name & Description \\
%				\hlx{vhv}
				\hlx{vhv}
				XMET & \# external methods called & HEFF & Halstead effort to implement\\
				VREF & \# variables referenced  & HDIF & Halstead difficulty to implement\\
				VDEC & \# variables declared  & EXCT & \# exceptions thrown\\
				%		TDN$*$ & Total depth of nesting\\
				NOS & \# statements  & EXCR & \# exceptions referenced\\
				NOPR & \# operators  & CREF & \# classes referenced\\
				NOA & \# arguments  & COMP & McCabes cyclomatic complexity\\
				%		NLOC$*$ & Number of lines of code\\
				NEXP & \# expressions  & CAST & \# class casts\\
				NAND & \# operands  & NBLTRL$*$ & \# Boolean literals\\
				%		MOD$*$ & Number of modifiers\\
				MDN & maximum depth of nesting  & NCLTRL$*$ & \# Character literals\\
				LOOP & \# loops (for,while)  & NSLTRL$*$ & \# String literals\\
				LMET & \# local methods called  & NNLTRL$*$ & \# Numerical literals\\
				%		HVOL$*$ & Halstead volume\\
				HVOC & Halstead vocabulary & NNULLTRL$*$ & \# Null literals\\
				%			HLTH$*$ & Halstead length of method\\
				
				%		HBUG$*$ & Halstead prediction of number of bugs\\
				%		NOC$*$ & Number of Comments\\
				%		NOCL$*$ & Number of Commented Lines\\
				
				\thickhline
			\end{tabular}
	%	}		
%	\end{center}
%	\vspace{-0.2in}
\end{table}

\subsubsection{Size Similarity Sharding} 
\label{sec:oreo_indexpar}
%As described before, applying a supervised learning technique to clone detection problem needs pairing each possible method with another in terms of a feature vector. This produces a huge number of candidates while many of them have a drastic difference and do not give useful information to the classification model. 

When doing clone detection on real code, the vast majority of method
pairs are {\em not} clones of each other. However, the clone detector
needs to process all possible pairs of methods in order to find out
which ones are clones. This can be extremely costly, and even
prohibitive on very large datasets, when the technique used for
detecting clones is CPU-intensive. A general strategy for speeding up
clone detection is to aggressively eliminate unlikely clone pairs
upfront based on very simple heuristics. We call the pairs which survive this aggressive elimination as \textit{candidate pairs}. Any method for which we are detecting clones is a \textit{query} and the methods which form a candidate pair with a \textit{query} are called \textit{candidate clones} of that \textit{query}.

The first, and simplest, heuristic used by \Name\ is size.  The
intuition is that two methods with considerably different sizes are
very unlikely to implement the same, or even similar,
functionality. This heuristic can lead to some false negatives,
specifically in the case of Type-4 clones. However, in all our
experiments, we observed little to no impact on the recall of other
clone types.%, especially those in the Twilight Zone.

As a metric of method size we use the number of tokens in the method,
where tokens are language keywords, literals (string literals are
split on whitespace), types, and identifiers. This is the same
definition used in other clone detection work
(e.g.~\cite{sajnani2016sourcerercc}). Given a similarity threshold
\textit{T} between 0 and 1, and a method $M_1$ with $x$ tokens, if a
method $M_2$ is a clone of $M_1$, then its number of tokens, $y$, should satisfy the inequation $ x \times T \leq y \leq \dfrac{x}{T}$.
%\begin{equation}\label{eq:indexpar}
%[x*T,\dfrac{x}{T}]
%\end{equation}

In \Name, this size similarity filter is implemented in the
preprocessing phase, by partitioning the dataset into shards based on
the size of the methods. We divide the dataset into multiple
partitions, such that each partition contains only methods within
certain lower and the upper size limits. In a partition, the methods with size between the upper and the lower size limits of the partition get to be the query methods. The partition's lower and
upper limits for candidate methods are calculated using the inequation given above, where $x$ is substituted with the
partition's lower and upper limits. The partitions are made such that
any given candidate method will at most belong to two partitions. The
remaining computations for clone detection are performed exclusively within
each of the shards.

Besides acting as a static filter for eliminating unlikely clones,
size-based sharding is also the basis for the creation of 
indexes that speed up clone detection in subsequent filters.

Another important design detail is that \Name\ uses a second-level
size-based sharding within each top-level shard, for the purpose of
loading batches of candidate pairs into memory. During clone
detection, we load each second-level shard into the memory one by one
and query it with all query methods in the shard's parent
partition. This leads to fast in-memory lookups, thereby increasing
the overall speed of clone-detection. The idea of input partitioning is not new, and has been used in
information retrieval systems many times
~\cite{livieri:2007icse,cambazoglu2006effect,kulkarni2010document}. Researchers
in other fields have explored partitioning based on size and also
horizontal partitioning to solve the scalability and speed
issues~\cite{liebeherr1993effect}. Here, we apply those lessons to our
clone detector.

%% Moreover, Svajlenko and Roy used
%% partitioning to do in-memory clone-detection and achieved very high
%% speed~\cite{svajlenko2017fast}. As far as we know, we are the first to
%% use two-level partitions of indexes for clone detection.


\subsubsection{Semantic Similarity: The Action Filter} 
\label{sec:action-filter}

%% \Name\ works by learning software metrics from code. The software metrics we use in \Name\ mostly capture structural properties of code and do not capture any semantics. Capturing semantical property of code is important if one wants to push the boundaries of clone detection from Type-3 towards Type-4.
%Hence, including a semantic feature would assure us that detected
%cloned methods are similar not only in terms of structural
%properties, but also in semantics. After exploring possible features,
%we could not find a meaningful semantical feature that has enough
%information gain for machine learning process. 
%% To this end, we decided to capture the semantic similarity one step
%% before preparing the metrics based feature vectors, and filter out
%% pairs that are not semantically similar before feeding them to the
%% main process. This filter, named \AF, 

Clones in the Twilight Zone have low lexical and syntactic similarity,
but still perform similar functions. In order to detect clones in this
spectrum, some sort of semantic comparison is necessary. We capture
the semantics of methods using a semantic signature consisting of what
we call \AT. The \AT\ of a method are the tokens corresponding to
methods called and fields accessed by that method. Additionally, we
capture array accesses (e.g. filename[i] and filename[i+1]) as
\textit{ArrayAccess} and \textit{ArrayAccessBinary} actions,
respectively. This is to capture this important semantic information
that Java encodes as syntax.

Semantic signatures are extracted during preprocessing. As an example
of \AT\ extraction, consider the code in Listing ~\ref{lst:actionfil},
which converts its input argument to an encrypted format. The
resulting \AT\ are: \textit{getBytes()}, \textit{getInstance()},
\textit{update()}, \textit{digest()}, \textit{length},
\textit{append()}, \textit{toString()}, \textit{traslate()},
\textit{ArrayAccess}, and \textit{toString()}.\footnote{The
  \textit{ArrayAccess} \textit{action token} stands for
  \textit{hashedPasswd[i]}.} More than the identifiers chosen by the developer, or the
types used, these \AT\ form a better semantic signature of the method. The intuition
is that if two methods perform the same function,
they likely call the same library methods and refer the same object
attributes, even if the methods are lexically and syntactically
different. Modern libraries provide basic semantic abstractions that
developers are likely to use; \Name\ assumes the existence and use of
these abstractions. Hence, we utilize these tokens to compare semantic
similarity between methods. This is done in the first dynamic filter
of \Name, the \AF. We use overlap-similarity, calculated as $Sim(A_1,A_2)= |A_1\cap A_2|$, to measure the similarity between the
\AT\ of two methods. Here, $A_1$ and $A_2$ are sets of
Action Tokens in methods $M_1$ and $M_2$ respectively. Each element in
these sets is defined as $<t,freq>$, where $t$ is the Action Token and
$freq$ is the number of times this token appears in the method. 

\begin{minipage}{0.95\textwidth}
\begin{lstlisting} [label={lst:actionfil},caption=Action Filter Example]

public static String getEncryptedPassword(String password) throws InfoException {
	StringBuffer buffer = new StringBuffer();
	try {
		byte[] encrypt = password.getBytes("UTF-8");
		MessageDigest md = MessageDigest.getInstance("SHA");
		md.update(encrypt);
		byte[] hashedPasswd = md.digest();
		for (int i = 0; i < hashedPasswd.length; i++) {
			buffer.append(Byte.toString(hashedPasswd[i]));
		}
	} catch (Exception e) {
	throw new InfoException(LanguageTraslator.traslate("474"), e);
}
return buffer.toString();
}
\end{lstlisting}
\end{minipage}

%\begin{equation}\label{overlap}
%Sim(A_1,A_2)= |A_1\cap A_2|
%\end{equation}

In order to speed up comparisons, we create an inverted index of all
the methods in a given shard using \AT. To detect clones for any
method, say M, in the shard, we query this inverted index for the
\AT\ of M. Any method, say N, returned by this query becomes a
candidate clone of M provided the overlap-similarity between M and N
is greater than a preset threshold. We call M the query method, N a
candidate of M, and the pair $<M,N>$ is called candidate pair.

Besides serving as semantic scanner of clone pairs, the \AF\ also
contributes to making the proposed approach both fast and
scalable because it eliminates, early on, candidate pairs for which the likelihood of being clones is low. 
%The \AF\ eliminates these pairs prior to further analysis by othercomponents of \Name.

% Moreover, the index creation needs no global information about the dataset, preserving the incremental nature of our approach.

Using the notion of method calls to find code similarity has been
previously explored by Goffi et al.~\cite{goffi2014search}, where
method invocation sequences in a code fragment are used to represent a
method. We are not interested in the sequence; instead, we use method
invocations in a bag of words model, which has been shown to
be robust in detecting Type-3 clones~\cite{sajnani2016sourcerercc}.


These concepts related to preprocessing, creating two level input partitioning, creating in-memory inverted indexes for fast lookups, and using Action tokens to filter out unlikely clone pairs help us in addressing candidate explosion problems in clone detection. The experiments to demonstrate scalability are presented later in Section~\ref{sec:oreo_eval_scalability}.



\subsection{Metrics Similarity} \label{sec:metrics}

Method pairs that survive the size filter and the \AF\ are passed on to
a more detailed analysis of their properties. In the case of \Name,
that detailed analysis focuses on the methods' software metrics. Here
we explain the reasons for this decision. The next section dives
deeper into the metrics similarity component.

Metrics based approaches for clone detection are known to work very
well if the goal is to find only Type-1 and Type-2
clones~\cite{mayrand1996experiment, patenaude1999extending,
  Kontogiannis:1997fk}. This is understandable-- given the strict
definitions of Type-1 and Type-2, the metric values of such clone
pairs should be mostly the same. For Type-3, metrics might look like a
good choice too because metrics are resilient to changes in
identifiers and literals. However, the use of metrics for clones in the Twilight Zone
is not straightforward, because these clones may be syntactically
different. As such, the use of metrics requires fine tuning over a
large number of configurations between the thresholds of each
individual metric. Finding the right balance manually can be hard: for
example-- is the number of conditionals more meaningful than the number of
arguments? To address this issue we use a supervised machine learning approach (Section~\ref{sec:oreo_approach}).%, explained in the next section.

The method pairs that reach the metrics filter are already known to be
similar in size and in their actions. The intuition for using metrics
as the final comparison is that methods that are of about the same
size and that do similar actions, but have quite different software
metrics characteristics are unlikely to be clones.

\subsection{Clone Detection Pipeline}
\label{sec:oreo_clonedetection}
Figure~\ref{fig:detect-pipeline} shows \Name's pipeline in more
detail, including the major intermediary data structures. This
pipeline makes use of all components presented in this section. Source code files are first given to a
\textit{Metrics Calculator} to extract methods and their software
metrics. These metrics form the input to \Name. Then, input
partitioning is conducted as described in Section~\ref{sec:oreo_indexpar},
which generates partitions containing query methods and possible
candidate methods. Then, for each partition, we create inverted index
of its candidates. This inverted index is further partitioned into
multiple shards, also explained in Section~\ref{sec:oreo_indexpar}. We then
load one of its index-shards into the memory, which is
queried with all of the queries in this partition.


\begin{figure*}
	\fbox{\includegraphics[width=.95\columnwidth] {Detection_pipeline_FSE.pdf}}
	\caption{Clone Detection Pipeline Process}
	\label{fig:detect-pipeline}
\end{figure*}


For each query method, the index returns a list of candidate clone
methods. Then, the hash values of the metrics for each query and its
candidates are compared. If metric hash of the query and a candidate
is equal, we report them as clones; this is because Type-1 and Type-2
clones have similar structures and thus, equal metric values. If the
metric hash is not equal, we pair the candidates with the query and
create feature vectors for the candidate pairs. These candidate pairs
are then analyzed by the trained model, which predicts if the pair is
a clone pair or not.  This process is repeated for all partitions and
their shards to identify all possible clone pairs. We describe the trained model used in \Name's pipeline in Section~\ref{sec:deeplearn}.
