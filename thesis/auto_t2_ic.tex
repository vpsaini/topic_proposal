\subsection{Automatic Resolution of \ttwo\ Clones}\label{sec:auto_t2}
To resolve \ttwo\ pairs automatically, we use two heuristics as described below:

\textbf{Action heuristic}: Action tokens of a method form a more stable semantic signature for the method than the identifiers or types chosen by the developer. This is because identifiers and types often change in duplicating methods, while Action tokens tend to remain the same. The reason is that methods and class attributes, represented by Action tokens, bring pre-implemented functionalities, which reduce the burden of coding, and hence, are not probable to be removed or modified after cloning. 

\textbf{Metric heuristic}: Software metrics, measuring different characteristics of source code, can capture structural information of a method. These measurements are resilient to changes in identifier names and literals -- a useful property in the detection of \ttwo\ clones. Hence, we use 24 method level software metrics shown in Table~\ref{tab:jhawk-metrics} for \ttwo\ resolution. The details of these metrics can be found elsewhere~\cite{Saini2018, oreopreprint}. A detailed explanation about the application of Action tokens and software metrics in clone detection can be found in~\cite{oreopreprint} and also in Section~\ref{sec:oreo_concepts}.


\begin{table}[!tbp]
	%\tiny
	\centering
%	\begin{center}
		\caption{Method-Level Software Metrics from \cite{oreopreprint}}
		\label{tab:jhawk-metrics}
	%	\resizebox{\linewidth}{!}{
			\begin{tabular} {l l l l}
				\midrule
			%	\hlx{v}
				Name & Description &Name & Description \\
				%				\hlx{vhv}
			%	\hlx{vhv}
				\midrule
				XMET & \# external methods called & HEFF & Halstead effort to implement\\
				VREF & \# variables referenced  & HDIF & Halstead difficulty to implement\\
				VDEC & \# variables declared  & EXCT & \# exceptions thrown\\
				%		TDN$*$ & Total depth of nesting\\
				NOS & \# statements  & EXCR & \# exceptions referenced\\
				NOPR & \# operators  & CREF & \# classes referenced\\
				NOA & \# arguments  & COMP & McCabes cyclomatic complexity\\
				%		NLOC$*$ & Number of lines of code\\
				NEXP & \# expressions  & CAST & \# class casts\\
				NAND & \# operands  & NBLTRL$*$ & \# Boolean literals\\
				%		MOD$*$ & Number of modifiers\\
				MDN & maximum depth of nesting  & NCLTRL$*$ & \# Character literals\\
				LOOP & \# loops (for,while)  & NSLTRL$*$ & \# String literals\\
				LMET & \# local methods called  & NNLTRL$*$ & \# Numerical literals\\
				%		HVOL$*$ & Halstead volume\\
				HVOC & Halstead vocabulary & NNULLTRL$*$ & \# Null literals\\
				%			HLTH$*$ & Halstead length of method\\
				
				%		HBUG$*$ & Halstead prediction of number of bugs\\
				%		NOC$*$ & Number of Comments\\
				%		NOCL$*$ & Number of Commented Lines\\
				
				\thickhline
			\end{tabular}
	%	}		
%	\end{center}
%	\vspace{-0.2in}
\end{table}

We use Algorithm~\ref{alg:autot2} to check if a candidate pair is a \ttwo\ clone. First,we get a list of action tokens for both methods \textit{(line 2 and 3)}. Then, we compare if these lists are identical \textit{(line 4)}, that is, the contents along with their order of appearance in these lists match. If the lists are identical, we get a list of metrics for both methods \textit{(line 5 and 6)} and then return true if these lists are identical \textit{(line 7)}, else return false. 

%The technique for resolving~\ttwo\ candidates is similar to the one
%used in Oreo~\cite{oreopreprint}: for both methods in the candidate
%pair, we compute and compare the hashes of their metrics; if two
%methods have an equal metric hashes, they are most likely
%\ttwo\ clones. The reasoning behind this technique is the following:
%since \ttwo\ clone pairs have an exactly similar structure
%and~\ttwo\ cloning does not involve any line-level modification,
%metrics for two code fragments in \ttwo\ cloning are equal.  However,
%since in this work we want to maximize precision, we cannot rely
%on a pure use of this technique as there could be methods which are
%not clones of each other but have the same metric hash. 
This algorithm ensures that a candidate pair is resolved as \ttwo\ only when there is a 100\% match in both the metrics and the Action tokens. The rational is that \ttwo\ clones differ in identifier names and literal values while their structure (captured by metrics), and their method calls and accessed class fields (captured using Action tokens) remain the same. 

% Setting Action filter similarity threshold to 100\% only ensures the structural similarity and therefore we need to employ additional methods to boost precision. To this end,
%we add a strong constraint: in addition to comparing the metric
%hashes, we also compare the action tokens of both methods in the
%candidate pair. Only when they have a 100\% match in their action
%tokens, the candidate pair is resolved as a \ttwo\ clone. 


\begin{algorithm}
	\scriptsize
	\textbf{INPUT:} $M1$ and $M2$ are strings representing the method bodies (including method signature) of two methods for which we want to know if they are \ttwo\ clones. 
	\textbf{OUTPUT:} $Boolean$ \\
	\begin{algorithmic}[1]
		\Function{IsTypeTwo}{$M1$, $M2$}
		\LState $ListATofM1$ = \Call{GetActionTokens}{$M1$}
		\LState $ListATofM2$ = \Call{GetActionTokens}{$M2$}	
		\If{ \Call{IsIdentical}{$ListATofM1$,$ListATofM2$}}
		\LState $ListMetM1$ = \Call{GetMetrics}{$M1$}
		\LState $ListMetM2$ = \Call{GetMetrics}{$M2$}
		\LState \Return \Call{IsIdentical}{$ListMetM1$,$ListMetM2$}
		\EndIf
		\LState \Return $False$
		\EndFunction 
	\end{algorithmic}
	\caption{\small Automatic \ttwo\ Resolution}
	\label{alg:autot2}
\end{algorithm}

