\chapter{Background and Related Work}
\label{chapter:background}


%\section{Why Do Code Clones Exist?}
\section{Existence of Code Cloning}
There has been substantial amount of research conducted to understand the reasons behind the existence of clones in software. The literature attributes the following reasons that lead to the existence of similar code fragments in software. 
\begin{itemize}
	\item \textbf{Reuse}.
	Developers often fork the repository of existing solutions and further modify it to meet the requirements of new but similar functionalities~\cite{kasper:2008}. This phenomena is not limited to forking the repositories but is well observed at other granularities e.g., copy-paste-modify of methods, classes, files, and packages, to name a few.
	\item \textbf{Maintenance Benefits}.
	Cloning offers many maintenance benefits such as separation of concern~\cite{rieger2005effective} and reusing an existing well-tested code~\cite{cordy:03}. It has been found that often clones diverge significantly from the original code fragment in terms of functionality~\cite{kasper:2008}. In such situations, cloning offers \textit{separation of concern}, a desirable design principle, where the original code and the duplicated code are to evolve at different paces. Moreover, the cloned code brings in the benefit of having already been tested as the original code fragment, thereby, reducing the risk of introducing a new code that can break the system.

\item \textbf{Language Limitations}.

Certain programming languages lack sufficient abstraction mechanisms, e.g., \textit{inheritance}, \textit{generic types}, and \textit{parameter passing}. This makes it difficult for developers to write a reusable code, which results into code duplication.
 
\item \textbf{API Usage Protocols}
Often developers need to adhere to a specific order in which the function calls are to be made while using certain libraries or APIs, resulting into accidental cloning. 

\item \textbf{Code Generation}
Frameworks and development environments often generate \textit{starter code}, enabling developers to expedite the implementation of functionalities. This causes the amount of boilerplate code becoming significantly large which results into unintentional clones~\cite{1553588}. 

\item \textbf{Fast Paced Development}
Certain software development practices require fast and frequent deliveries. Writing a reusable code under such circumstances is not only time-consuming but is also error prone. Developers often copy-paste-modify existing codes to meet time-pressed deadlines.

\end{itemize}




\section{Applications of Clone Detection}
Knowing where clones exist in, or across, systems leads to many useful applications:


\begin{itemize} 
\item \textbf{Software Maintenance}
The clones detected can be re-factored into reusable components, which often is considered as an improvement in the software design. Refactoring leads to lower maintenance costs as there are fewer instances in the code that need to be maintained. In a recent survey 
Chatterji et al. finds that clone management is useful for maintenance tasks and affects long term system quality~\cite{Chatterji:2016:CCD:2967712.2967742}.

\item \textbf{Library Candidate Identification}
Clone detection helps in identifying library candidates which in turn improves re-usability and lowers maintenance cost. For example, a code fragment which is cloned multiple times, proves its usability and therefore is a good candidate to be included in a library as an API. Kawrykow and Robillard use code similarity to find instances of code that is already available in the form of APIs, also known as API imitation~\cite{Kawrykow:2009:IAU:1747491.1747513}. In their study on ten Java projects, they found
400 cases of API imitations. They argue that developers could improve the quality of their software by removing such instances of API imitation.  

\item \textbf{Plagiarism Detection and Copyright Infringement}
Some of the most popular and practical uses of clone detection are to identify plagiarized code and unauthorized usage of software (license violation)~\cite{Zoranplag}. Software plagiarism happens when someone copies a code and uses it in a way that it hides the ownership of the copied code. In a famous case between Oracle and Google, Oracle accused Google of copying and using parts of code from twelve source files and 37 Java specification in their Java APIs in the Android operating
system.


\item \textbf{Reverse Engineering product line architecture}
Identification of common components across multiple source code repositories can help in extracting the common features in a product and its derivatives~\cite{5069483, hemel:2012wcre}. This reverse engineering process helps in creating more streamlined product pipeline.

\item \textbf{Dataset Curation}
Large datasets of source code often contain a large amount of clones at different granularities such as method, class, file, and project to name a few. Empirical studies conducted over such datasets could get adversely affected by the amount of duplicate information present in the datasets. A modern usecase of clone detection is to curate large datasets that are void of clones~\cite{lopes:oopsla17}.

\item \textbf{Facilitating Research in Software Engineering}
Clone detection has been successfully used in different areas of research pertaining to software engineering. For example, software evolution analysis, aspect mining research, program comprehension, software provenance analysis and code search to name a few~\cite{lopes:oopsla17, Yang:2017:SOG:3104188.3104224, beck:2010wc,kim:2005}.

\end{itemize}

\section{Clone Detection Techniques and Tools}
 Clone detectors differ substantially in the underlying techniques and scope of application. In terms of technical approach used, one can find techniques that are token-based~\cite{li2006cp,sajnani2016sourcerercc,svajlenko2017fast}, metrics-based~\cite{mayrand1996experiment,Kontogiannis:1997fk}, learning-based~\cite{white2016deep,oreopreprint},  tree-based~\cite{jiang2007deckard,baxter1998clone}, graph-based~\cite{gabel2008scalable}, and text-based~\cite{roy2008nicad}. A detailed description of these techniques can be found elsewhere~\cite{roy:queens:07}. The techniques that are most related to my work are described as follows.
 
 \begin{itemize}
\item \textbf{Token-based Techniques}
Token-based techniques use sophisticated transformations on sourcecode such as using lexical analyzers to generate a stream of tokens. These techniques compare tokens of code fragments in order to determine whether the two fragments are clones or not. Token-based techniques when used in a bag-of-words model, are demonstrated to be resilient in detecting near-miss \tthree\ clones. Sajnani et al. demonstrated that token-based techniques can be scaled to large datasets containing millions of lines of code~\cite{sajnani2016sourcerercc}. CCFinderX~\cite{kamiya:2002zr}, SourcererCC~\cite{sajnani2016sourcerercc}, and CloneWorks~\cite{svajlenko2017cloneworks} are examples of popular token-based clone detectors.

SourcererCC~\cite{sajnani2016sourcerercc} and CloneWorks~\cite{svajlenko2017fast} have shown good accuracy in detecting \tone\ to near-miss \tthree\ clones. SourcererCC and CloneWorks create a global token frequency map of all the tokens in the target dataset. They then use this global token frequency map to create an inverted partial-index of code blocks. Clone detection is carried out by querying the partial-index. CloneWorks differs from SourcererCC by using a modified Jaccard similarity metric in detecting clones, whereas the latter uses Overlap similarity metric. 

 \item \textbf{Metrics-based Techniques}
 Metrics-based techniques compute and compare different software metrics of two code fragments in order to determine if the fragments are clones or not. Such techniques instead of comparing codes directly, compare these metrics. Two code fragments are considered clones when their corresponding metrics value are similar. Metrics-based techniques when applied to \tthree\ clone detection suffer from high false positive rates and scalability issues. 
 
 Keivanloo et al. ~\cite{keivanloo2012java} introduced a hybrid metrics-based approach to detect semantic clones from Java Bytecode. This approach utilizes four metrics: two numerical metrics which are Java type Jaccard similarity, and Method similarity; and two ordinal metrics which are Java type pattern and Java method pattern. %They have evaluated their technique on 300 KLOC.
 Mayrand et al.~\cite{mayrand1996experiment} used Datrix tool to calculate 21 metrics for functions. They compare these metric values, and then, functions with similar metric values are identified as clones. The similarity threshold was manually decided. A similar technique is used by Patenaude et al. to find method level clones in Java projects~\cite{patenaude1999extending}.
 These metrics are compared across four points of comparisons namely: Name, Layout, Expressions, and Control flow. Two functions are reported as similar for a point of comparison only if the absolute difference is less than or equal to the delta threshold defined for each metric in that point of comparison. The deltas were defined on the basis of metric definition and their knowledge of the distribution of that metric on large scale systems. 
 
 Kontogiannis et al. ~\cite{Kontogiannis:1997fk} used five modified versions of well known metrics that capture data and control flow properties of program to detect clones in the granularity of begin -- end blocks. They experimented with two techniques. The first one is based on the pairwise Euclidean distance comparison of begin-end blocks. The second technique uses clustering. Each metric is seen as an axis and clustering is performed per metric axis. Intersection of clusters results into the clone fragments. 
 
 \item \textbf{Learning-based Techniques}
 In learning-based techniques, features are extracted from the code fragments. These features are then used to train a machine learning model to detect clones. These learning techniques can be either supervised or unsupervised. Like metrics-based techniques, these techniques suffer from scalability challenges. Usually, the features extracted represent the structural information of the code and lack in semantic information, which in turn leads to a high false positive rate.
 
Davey et al. ~\cite{davey1995development} attempt to detect clones by training neural networks on certain features of code blocks. They create feature vectors based on indentations of each line in the code-block, length of each line in the code-block, and frequency of keywords used in the code block.

 White et
 al.~\cite{white2016deep} present an unsupervised deep learning
 approach to detect clones at method and file levels. They explored the
 feasibility of their technique by automatically learning
 discriminating features of source code. They report the precision of
 their approach to be 93\% on 398 files and 480 method-level pairs
 across eight Java systems. 
 
 Sheneamer and Kalita use AST and PDG based techniques to generate semantic and syntactic features to train their model~\cite{Sheneamer:icmla16} to detect clones.
 
 In Wei and Li's approach ~\cite{ijcai2017-423}, a
 Long-Short-Term-Memory network is applied to learn representations of
 code fragments and also to learn the parameters of a hash function
 such that the Hamming distance between the hash codes of clone pairs
 is very small.
 
 CCLearner~\cite{li2017cclearner} is another approach that leverages deep learning to classify clone and non-clone method pairs. In this work, authors have used the information about the usage of terms in source code (such as reserved words or identifiers) to build the feature vector for each method.
 
 
 
\end{itemize}
 
%\section{Impact of Code Cloning on Software Systems}
%\section{SourcererCC}


\section{Evaluation of Clone Detection Techniques}
Clone detection tools and techniques can be compared using several parameters. These parameters can also be seen as the desirable properties of clone detection approaches. In the following we list some of the well-known parameters used in the literature.

\begin{itemize}
	\item \textbf{Recall.}
	Recall is the fraction of true positives that have been retrieved over the total amount of true positives in the dataset. An ideal clone detector should be capable of detecting all of the clone pairs in a given dataset.
	 
	\item \textbf{Precision.}
	Precision is the fraction of true positives retrieved among the total retrieved instances.An ideal clone detector should contain no false positives in its output.
	
	\item \textbf{Scalability.}
	A good clone detector should be able to detect clones in a large dataset with reasonable memory usage.
	
	\item \textbf{Execution Time.}
	A good clone detector should complete the execution in a reasonable amount of time depending on the size of input.
	
	\item \textbf{Robustness.}
	A good clone detector should be able to detect clones of different types with high precision and recall.
	
	\item \textbf{Portability.}
	A good clone detector should have minimum dependency on the target platform or language and should be easy enough to adapt to various languages.
	
\end{itemize}

The effectiveness of clone detection tools is usually evaluated in terms of precision and recall. There has been good progress in measuring recall
systematically of clone detection tools. Svajlenko et al. created BigCloneBench, a dataset of real world Java clones~\cite{Svajlenko:2014:TBD:2705615.2706134}. They also created BigCloneEval, a tool that estimates recall of clone detectors based on BigCloneBench dataset by
measuring how many of the labeled clone pairs are included in the
output of a clone detector~\cite{7816515}.

Measuring precision of a clone detector, however, is not simple. To measure the precision, one can choose to manually validate all the clone pairs reported by a clone detector. This process is extremely time consuming and impractical as the number of clone pairs reported by a tool on a standard dataset like BigCloneBench is in millions. A more practical process is to estimate the precision by humanly validating a random and statistically significant sample of clone pairs. This is what researchers do to estimate the precision of clone detectors~\cite{sajnani2016sourcerercc,Svajlenko:2015:ECD:2881297.2881379,Wang:2018:CTB:3180155.3180179,oreopreprint}. In this process, after running a clone detector on a dataset and getting the clone pairs, a random and statistically significant sample set of these clone pairs is assigned to multiple judges for manual
inspection. The judges examine each pair to decide if it is a true clone
and/or what type of clone it is. When all sampled pairs have been
validated by all judges, researchers aggregate the judges'
decisions, usually by taking the majority vote, and report precision. 


\section{Chapter Summary}
This chapter gave an overview of the research in the area of clone detection. The chapter looked into the reasons behind why clones exist in software systems. Cloning could be intentional or unintentional. Reusing existing code in the form of copy-paste-modify to meet time-pressed deadlines and to avoid introducing new bugs through new code are some of the reasons why developers make clones. On the other hand, clones may exist out of the limitations of language and protocols of API usage as well as through automatic code generators.

Clone detection has many applications which span both industry as well as research. Some of the popular applications of clone detection are: library candidate identification, plagiarism detection, copyright infringement detection, dataset curation and facilitating research in software engineering.

 Over the years, many tools and techniques have been proposed to detect clones in software. In the literature, several techniques are proposed that are token-based, metrics-based, learning-based, tree-based, graph-based and text-based. These techniques are compared based on parameters such as: recall, precision, scalability, execution time, robustness and portability.
 
 
